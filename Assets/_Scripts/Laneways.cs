using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class Laneways : MonoBehaviour
{


	void
	Start()
	{
		if (GameObject.FindObjectOfType<GameplaySettings> () == null)
		{
			SceneManager.LoadScene ("Title Screen");
		}
	}
	float nextSpawnTime = 0;

	[SerializeField] List<RectTransform> laneways = new List<RectTransform>();


	[SerializeField] RectTransform lanePrefab;

	bool lanesReady = false;

	GameplaySettings settings;

	bool libraryCompletion;

	bool timedRound;
	float startTime = 0f;
	float endTime = 0f;

	int wordsLeft;

	[SerializeField] Text timerText;
	[SerializeField] Text wordCounterText;
	[SerializeField] Text libraryCounterText;

	bool paused = false;


	//float timeTaken = 0f;

	public void
	Initialise (GameplaySettings newSettings)
	{
		settings = newSettings;
		for (int i = 0; i < settings.activeLibraries.Count; i++)
		{
			libraryRatings.Add (new LibraryRating ());

			RectTransform newLane = Instantiate (lanePrefab);
			newLane.SetParent (transform, false);
			newLane.SetAsFirstSibling ();

			float x = GetComponent<RectTransform> ().sizeDelta.x / settings.activeLibraries.Count;
			float y = GetComponent<RectTransform> ().sizeDelta.y;

			newLane.sizeDelta = new Vector2 (x, y);
			newLane.GetChild(0).GetComponent<RectTransform> ().sizeDelta = new Vector2 (newLane.sizeDelta.x, newLane.GetChild(0).GetComponent<RectTransform>().sizeDelta.y);
			newLane.GetChild(1).GetComponent<RectTransform> ().sizeDelta = new Vector2 (newLane.sizeDelta.x, newLane.GetChild(1).GetComponent<RectTransform>().sizeDelta.y);
			newLane.anchoredPosition = new Vector2 (i * x, 0);

			newLane.GetComponent<Image> ().color = settings.laneColours [i];
			newLane.GetChild (1).GetComponent<Image> ().color = settings.laneColours [i];

			newLane.GetComponent<Lane> ().SetLibrary (settings.activeLibraries [i], i);

			laneways.Add (newLane);

			laneMarker.sizeDelta = new Vector2 (x, laneMarker.sizeDelta.y);



			libraryCompletion = settings.libraryCompletion;
			wordsLeft = settings.wordCountLength;

			libraryCounterText.gameObject.SetActive(settings.libraryCompletion);
			wordCounterText.gameObject.SetActive(settings.wordCount);


			score = 0;
			totalPhrases = 0;
			translatedPhrases = 0;
			completedPhrases = 0;
			missedPhrases = 0;

			GetComponent<PowerupInput> ().Initialise ();

			startTime = Time.time;
			endTime = Time.time + settings.timerLength;

			timedRound = settings.timedRound;

			gameOver = false;

		}

		timerText.gameObject.SetActive (settings.timedRound);

		lanesReady = true;
		CloseLanewaysExcept (0);
		Unpause ();


		//ResizeAllLanes ();
	}


	public void
	Retry()
	{
//		foreach (RectTransform lane in laneways)
//		{
//			GameObject.Destroy (lane.gameObject);
//		}
//
//		laneways.Clear ();
//
//		Initialise (GameObject.FindObjectOfType<GameplaySettings> ());
		//THESE LINES DISABLED BECAUSE RATING SPRITES DON'T RESET YET

		RetryPlaceholder ();

	}

	public void
	RetryPlaceholder()
	{
		SceneManager.LoadScene ("Main Scene");
	}

	void
	ResizeAllLanes()
	{
		foreach (RectTransform rect in laneways)
		{
			rect.sizeDelta = new Vector2 (1920 / laneways.Count, 1080);
			rect.anchoredPosition = new Vector2 (laneways.IndexOf(rect) * rect.sizeDelta.x, 0);
			rect.GetChild (0).GetComponent<RectTransform> ().sizeDelta = new Vector2 (rect.sizeDelta.x, rect.GetChild(0).GetComponent<RectTransform>().sizeDelta.y);
			rect.GetChild (1).GetComponent<RectTransform> ().sizeDelta = new Vector2 (rect.sizeDelta.x, rect.GetChild(1).GetComponent<RectTransform>().sizeDelta.y);
		}
	}

	int
	CheckAmountOfWords()
	{
		int words = GameObject.FindObjectsOfType<TextTarget> ().Length;
		return words;
	}

	void
	Update()
	{
		if (!gameOver)
		{
			ReleaseNewWord ();

			if (Input.GetKeyDown ("1"))
			{
				CloseLanewaysExcept (0);
			}
			else if (Input.GetKeyDown ("2"))
			{
				CloseLanewaysExcept (1);
			}
			else if (Input.GetKeyDown ("3"))
			{
				CloseLanewaysExcept (2);
			}
			else if (Input.GetKeyDown ("4"))
			{
				CloseLanewaysExcept (3);
			}
			else if (Input.GetKeyDown ("5"))
			{
				CloseLanewaysExcept (4);
			}

			if (Input.GetKeyDown ("left") || (Input.GetKeyDown("tab") && (Input.GetKey("left shift") || Input.GetKey("right shift"))))
			{
				if (currentLane == 0)
					currentLane = 4;
				else
					currentLane--;

				CloseLanewaysExcept (currentLane);
			}
			else if (Input.GetKeyDown ("right") || (Input.GetKeyDown("tab") && !Input.GetKey("left shift") && !Input.GetKey("right shift")))
			{
				if (currentLane == laneways.Count - 1)
					currentLane = 0;
				else
					currentLane++;

				CloseLanewaysExcept (currentLane);
			}


			if (Input.GetKeyDown ("escape"))
			{
				Pause ();
			}



			CheckTimer ();
			CheckActiveLanes ();
			CheckWordCount ();
		}
	}

	void
	CheckWordCount()
	{
		wordCounterText.text = wordsLeft.ToString () + " words left";
		if (settings.wordCount && wordsLeft < 1)
		{
			DisplayCompleteScreen ();
		}
	}

	void
	CheckTimer()
	{
		if (!paused)
		{
			int timeLeft = (int)(endTime - Time.time);
			timerText.text = timeLeft.ToString () + " seconds left";
			if (timedRound)
			{
				if (Time.time > endTime)
				{
					DisplayCompleteScreen ();
				}
			}
		}
	}

	public bool
	VisibleWords()
	{
		List<TextTarget> words = new List<TextTarget>(GameObject.FindObjectsOfType<TextTarget> ());

		foreach (TextTarget word in words)
		{
			if (word.GetComponent<RectTransform> ().anchoredPosition.y <= -180)
			{
				return true;
			}
		}
		return false;
	}

	void
	CheckActiveLanes()
	{
		bool activeLaneExists = false;
		int activeLaneCount = 0;

		foreach (RectTransform lane in laneways)
		{
			if (lane.GetComponent<Lane> ().GetActive ())
			{
				activeLaneExists = true;
				activeLaneCount++;
			}
		}

		libraryCounterText.text = activeLaneCount.ToString () + " libraries left";

		if (!activeLaneExists)
		{
			DisplayCompleteScreen ();
		}
	}

	float tempScrollSpeed = 0f;
	float pauseTime;

	void
	Unpause()
	{
		if (paused)
			Pause ();
	}

	public void
	Pause()
	{
		if (!paused)
		{
			pauseTime = Time.time;
			tempScrollSpeed = settings.currentScrollSpeed;
			settings.currentScrollSpeed = 0f;
		}
		else
		{
			endTime += Time.time - pauseTime;
			nextSpawnTime += Time.time - pauseTime;
			settings.currentScrollSpeed = tempScrollSpeed;
		}

		paused = !paused;

		if (paused)
		{
			timerText.text = "Paused";
		}
	}







	//COMPLETE SCREEN//COMPLETE SCREEN//COMPLETE SCREEN//COMPLETE SCREEN//COMPLETE SCREEN//COMPLETE SCREEN//COMPLETE SCREEN//COMPLETE SCREEN

	[SerializeField] LerpScreen completeScreen;
	[SerializeField] ActiveLibraries completeLibraryDisplay;
	[SerializeField] Text wordsCompleteText;
	[SerializeField] Text wordsMissedText;
	[SerializeField] Text timeTakenText;


	float score;
	public int totalPhrases = 0;
	int translatedPhrases = 0;
	int completedPhrases = 0;
	int missedPhrases = 0;

	[SerializeField] Sprite cuplaFocalSprite;
	[SerializeField] Sprite fainneAirgidSprite;
	[SerializeField] Sprite fainneOirSprite;
	[SerializeField] Sprite seanFhainneSprite;

	[SerializeField] Image cuplaFocal;
	[SerializeField] Image fainneAirgid;
	[SerializeField] Image fainneOir;
	[SerializeField] Image seanFhainne;

	[SerializeField] Text phrasesTranslatedText;

	List<LibraryRating> libraryRatings = new List<LibraryRating>();


	public void
	IncrementTranslatedPhrases(int i)
	{
		if (settings.activeLibraries [i].CheckForAllCases ().prompt != null)
		{
			translatedPhrases++;
			libraryRatings [i].translatedPhrases++;
		}
	}

	public void
	IncrementMissedPhrases(int i)
	{
		if (settings.activeLibraries [i].CheckForAllCases ().prompt != null)
		{
			missedPhrases++;
			libraryRatings [i].missedPhrases++;
		}
	}

	public void
	IncrementCompletePhrases(int i)
	{
		if (settings.activeLibraries [i].CheckForAllCases ().prompt != null)
		{
			completedPhrases++;
			libraryRatings [i].completedPhrases++;
			wordsLeft--;
		}
	}

	public void
	IncrementTotalPhrases(int amount, int index)
	{
		if (settings.activeLibraries [index].CheckForAllCases ().prompt != null)
		{
			totalPhrases += amount;
			libraryRatings [index].totalPhrases += amount;
		}
	}

	void
	CSCalculateScore()
	{
		score = completedPhrases;
		score -= (missedPhrases * 2);
		score -= (translatedPhrases);

		score = score / totalPhrases;
		score = score * 100;

		float multiplier = 1f;//Mathf.Clamp(settings.currentFrequency / 300, 0.75f, 1.25f);

		score = score * multiplier;

		CSDisplayScoreUI (score);

		for (int i = 0; i < libraryRatings.Count; i++)
		{
			int b = i;
			float newScore = libraryRatings [i].CalculateScore (multiplier);

			//[flag]these newratings should really be calculated in NewRating()
			if (newScore > 90)
			{
				settings.activeLibraries [i].NewRating (newScore, RatingType.SeanFhainne);
				b = GameObject.FindObjectsOfType<SetScore> ().Length - 1 - b;
				GameObject.FindObjectsOfType<SetScore>()[b].GoGo(RatingType.SeanFhainne);
			}
			else if (newScore > 75)
			{
				settings.activeLibraries [i].NewRating (newScore, RatingType.FainneOir);
				b = GameObject.FindObjectsOfType<SetScore> ().Length - 1 - b;
				GameObject.FindObjectsOfType<SetScore>()[b].GoGo(RatingType.FainneOir);
			}
			else if (newScore > 55)
			{
				settings.activeLibraries [i].NewRating (newScore, RatingType.FainneAirgid);
				b = GameObject.FindObjectsOfType<SetScore> ().Length - 1 - b;
				GameObject.FindObjectsOfType<SetScore>()[b].GoGo(RatingType.FainneAirgid);
			}
			else if (newScore > 10)
			{
				settings.activeLibraries [i].NewRating (newScore, RatingType.CuplaFocal);
				b = GameObject.FindObjectsOfType<SetScore> ().Length - 1 - b;
				GameObject.FindObjectsOfType<SetScore>()[b].GoGo(RatingType.CuplaFocal);
			}
			if (settings.activeLibraries [i].CheckForAllCases ().prompt == null)
			{
				settings.activeLibraries [i].NewRating (newScore, RatingType.Unscored);
				b = GameObject.FindObjectsOfType<SetScore> ().Length - 1 - b;
				GameObject.FindObjectsOfType<SetScore> () [b].GoGo (RatingType.Unscored);
			}

		}
	}

	void
	CSDisplayScoreUI(float score)
	{
		if(score > 0)
		{
			cuplaFocal.sprite = cuplaFocalSprite;
			cuplaFocal.color = new Color (1, 1, 1, 1);
		}
		if(score > 55)
		{
			fainneAirgid.sprite = fainneAirgidSprite;
			fainneAirgid.color = new Color (1, 1, 1, 1);
		}
		if(score > 75)
		{
			fainneOir.sprite = fainneOirSprite;
			fainneOir.color = new Color (1, 1, 1, 1);
		}
		if(score > 90)
		{
			seanFhainne.sprite = seanFhainneSprite;
			seanFhainne.color = new Color (1, 1, 1, 1);
		}
	}


	public void
	DisplayCompleteScreen()
	{
		gameOver = true;

		wordsCompleteText.text = completedPhrases.ToString();
		wordsMissedText.text = missedPhrases.ToString();
		phrasesTranslatedText.text = translatedPhrases.ToString ();

		float minutesTaken = (Time.time - startTime) / 60;
		float secondsTaken = (Time.time - startTime) % 60;

		timeTakenText.text = (int)minutesTaken + " mins " + (int)secondsTaken + " secs";

		completeLibraryDisplay.SetSettings (settings);
		completeLibraryDisplay.DisplayActiveLibraries ();
		completeScreen.OpenMenu (true);

		foreach (Library library in settings.activeLibraries)
		{

			//library.timesCompleted++;//can be moved to score calculation[flag]

		
		}

		CSCalculateScore ();
	}

	bool gameOver;

	public void
	LeaveToTitle()
	{
		Destroy (settings.gameObject);
		SceneManager.LoadScene ("Title Screen");
	}

	int lastWordCharCount = 0;

	void
	ReleaseNewWord()
	{
		if (lanesReady && CheckAmountOfWords () < settings.maxWords && !paused)
		{
			if (nextSpawnTime < Time.time)
			{
				List<Lane> notBusyLanes = GetActiveLanes ();
				if (notBusyLanes.Count > 0)
				{
					lastWordCharCount = notBusyLanes [Random.Range (0, notBusyLanes.Count)].CreateNewWord ();
				}
				else
				{
					lastWordCharCount = 0;
				}

				//lastWordCharCount = laneways [Random.Range (0, laneways.Count)].GetComponent<Lane> ().CreateNewWord ();
				nextSpawnTime = Time.time + GetWaitTimeForNewWord();
			}
		}
	}


	List<Lane>
	GetActiveLanes()
	{
		List<Lane> notBusyLanes = new List<Lane> ();

		foreach (Transform lane in laneways)
		{
			if (lane.GetComponent<Lane> ().CheckReadyToMakeWord())
			{
				//print ("Ready to make a word!");
				notBusyLanes.Add (lane.GetComponent<Lane> ());
			}
		}

		return notBusyLanes;
	}

	float
	GetWaitTimeForNewWord()
	{
		if (lastWordCharCount != 0)
		{
			lastWordCharCount = Mathf.Clamp (lastWordCharCount, 1, 100);//maximum supported characters in a word?
			return 60f / (settings.currentFrequency / lastWordCharCount) + .5f;
		}
		else
			return 0;
	}


	int currentLane = 0;

	void
	CloseLanewaysExcept(int i)
	{
		int clampedIndex = Mathf.Clamp (i, 0, laneways.Count - 1);

		foreach (RectTransform rect in laneways)
		{
			rect.GetComponent<Lane> ().SetAsCurrentLane (false);
		}

		laneways [clampedIndex].GetComponent<Lane> ().SetAsCurrentLane (true);
		currentLane = clampedIndex;

		SetLaneMarkerPosition (clampedIndex);
	}

	[SerializeField] RectTransform laneMarker;

	void
	SetLaneMarkerPosition(int i)
	{
		laneMarker.anchoredPosition = new Vector2 (i * (1920 / laneways.Count), laneMarker.anchoredPosition.y);
	}
}

public class LibraryRating : System.Object
{
	public int completedPhrases;
	public int missedPhrases;
	public int translatedPhrases;
	public int totalPhrases;

	public float score;

	public float
	CalculateScore(float multiplier)
	{
		score = completedPhrases;
		score -= (missedPhrases * 2);
		score -= (translatedPhrases);

		score = score / totalPhrases;
		score = score * 100;

		score = score * multiplier;
		return score;
	}
}