﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PowerupInput : MonoBehaviour
{
	[SerializeField] Text timerText;
	[SerializeField] Image freezeIcon;


	float freezeTime;
	bool isFrozen = false;

	void
	Start()
	{
		SetUI ();
	}

	public void
	Initialise()
	{
		freezeTime = 30f;
		SetUI ();
	}

	void
	Update ()
	{
		FreezePowerup();
	}


	void
	FreezePowerup()
	{
		if (Input.GetButtonDown ("Freeze") && freezeTime > 0f)
		{
			FreezeAllLanes ();
		}
		else if (isFrozen && freezeTime >= 0f)
		{
			freezeTime -= Time.deltaTime;
			SetUI ();
		}
		else if (isFrozen && freezeTime < 0f)
		{
			FreezeAllLanes ();
		}
		if (isFrozen && !GetComponent<Laneways> ().VisibleWords ())
		{
			FreezeAllLanes ();
		}
	}


	void
	FreezeAllLanes()
	{
		isFrozen = !isFrozen;

		foreach (Lane lane in transform.GetComponentsInChildren<Lane>())
		{
			lane.Freeze (isFrozen);
		}

		SetUI ();
	}


	void
	SetUI()
	{
		freezeIcon.gameObject.SetActive (isFrozen);
		if (freezeTime > 0f)
		{
			
			timerText.text = ((float)((int)(freezeTime * 10)) / 10).ToString ();
		}
		else
		{
			timerText.text = "0";
		}

		if (!timerText.text.Contains ("."))
		{
			timerText.text += ".0";
		}
	}

	public void
	IncreaseFreezeTime(float amount)
	{
		freezeTime += amount;
		SetUI ();
	}
}