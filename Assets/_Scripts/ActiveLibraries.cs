﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ActiveLibraries : MonoBehaviour
{
	[Header("Prefabs")]
	[SerializeField] RectTransform lanePreviewPrefab;

	[Header("References")]
	[SerializeField] GameplaySettings settings;

	void
	Awake()
	{
		if (settings != null)
		settings.OnLibraryUpdated += DisplayActiveLibraries;
	}

	void
	OnDestroy()
	{
		if (settings != null)
		settings.OnLibraryUpdated -= DisplayActiveLibraries;
	}

	public void
	SetSettings(GameplaySettings newSettings)
	{
		settings = newSettings;
	}

	public void
	DisplayActiveLibraries ()
	{
		foreach (Transform oldLibrary in transform)
		{
			if(oldLibrary.GetComponent<Text>() == null)
			Destroy (oldLibrary.gameObject);
		}

		if (settings.activeLibraries.Count > 0)
		{
			float width = GetComponent<RectTransform> ().sizeDelta.x / settings.activeLibraries.Count;

			for (int i = 0; i < settings.activeLibraries.Count; i++)
			{
				RectTransform newLanePreview = Instantiate (lanePreviewPrefab);
				newLanePreview.SetParent (transform, false);
				newLanePreview.anchoredPosition = new Vector2 (i * width, 0);
				newLanePreview.sizeDelta = new Vector2 (width, GetComponent<RectTransform> ().sizeDelta.y);

				newLanePreview.GetComponent<Image> ().color = settings.laneColours[i];

				newLanePreview.GetChild (0).GetComponent<Text> ().text = settings.activeLibraries [i].title;
				newLanePreview.GetChild (1).GetComponent<Text> ().text = settings.activeLibraries [i].author;

//				if(newLanePreview.GetComponent<SetScore> ())
//				{
//					newLanePreview.GetComponent<SetScore> ().GoGo (settings.activeLibraries [i].rating);
//				}

				if (newLanePreview.GetComponent<DirectoryPreview> ())
				{
					newLanePreview.GetComponent<DirectoryPreview>().myLibrary = settings.activeLibraries[i];
				}
			}
		}
	}
}