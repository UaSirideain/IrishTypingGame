﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class TextTarget : MonoBehaviour
{
	GameplaySettings settings { get { return GameObject.FindObjectOfType<GameplaySettings> (); } }

	int currentLetter = 0;
	string currentLetterC;
	[SerializeField] Text targetWord;

	[SerializeField] Text writtenText;
	[SerializeField] Vector2 movement;
	[SerializeField] float secondsToBottom;

	bool isMoving = true;

	Phrase word;
	[SerializeField] string wordToRead;
	[SerializeField] public string wordToType;


	[SerializeField] GameObject letterCompletePrefab;

	[SerializeField] public float scrollDistancePerSecond = 0f;
	[SerializeField] float minScrollSpeed;
	[SerializeField] float maxScrollSpeed;

	DisplayLanguage displayLanguage;

	int libraryIndex;

	public void
	Initialise(Phrase newWord, int newLibraryIndex)
	{
		word = newWord;
		libraryIndex = newLibraryIndex;

		//myIndex = wordIndex;

		displayLanguage = settings.displayLanguage;

		if (newWord.targetOnly)
		{
			displayLanguage = DisplayLanguage.Target;
		}

		if (displayLanguage == DisplayLanguage.Target)
		{
			wordToRead = word.target;
		}
		else
		{
			wordToRead = word.prompt;
		}

		if (displayLanguage == DisplayLanguage.Both || displayLanguage == DisplayLanguage.Target)
		{
			GameObject.FindObjectOfType<Laneways> ().IncrementTranslatedPhrases (libraryIndex);
		}

		wordToType = word.target;
		targetWord.text = wordToRead;


		GetComponent<RectTransform> ().sizeDelta = new Vector2((1920f / settings.activeLibraries.Count) - 30, 70);
		transform.GetChild(0).GetComponent<RectTransform> ().sizeDelta = new Vector2((1920f / settings.activeLibraries.Count) - 30, 70);
		transform.GetChild (0).GetComponent<RectTransform> ().offsetMin = new Vector2(0, transform.GetChild (0).GetComponent<RectTransform> ().offsetMin.y);
		transform.GetChild (0).GetComponent<RectTransform> ().offsetMax = new Vector2(0, transform.GetChild (0).GetComponent<RectTransform> ().offsetMax.y);

		if (word.prompt.Length > 10)
		{
			targetWord.fontSize -= 8;
		}
		if (word.target.Length > 10)
		{
			writtenText.fontSize -= 8;
		}
			
		if(displayLanguage == DisplayLanguage.Both)
		{
			UpdateWordAppearance (writtenText);
			writtenText.color = new Color (.35f, .35f, .35f, 1f);
		}


		scrollDistancePerSecond = GetScrollSpeed();
		movement = new Vector2(0f, -scrollDistancePerSecond);

		interChars = settings.GetComponent<InterchangableCharacters> ();
	}

	public float
	GetScrollSpeed()
	{
		float distanceToCover = 800f;
		float wordLifeTime = transform.parent.GetComponent<Lane>().wordLifeTime;
		float speed = distanceToCover / wordLifeTime;

		return  Mathf.Clamp (speed, minScrollSpeed, maxScrollSpeed) * .57f;
	}


	bool dead = false;

	void
	Update()
	{
		if (transform.localPosition.y < -880 && !dead)
		{
			dead = true;
			CompleteWord (false);//being called a billion times
		}

		CompleteDisregardedCharacters ();
	}

	[SerializeField] InterchangableCharacters interChars;

	void
	CompleteDisregardedCharacters()
	{
		if (wordToType != null)
		if(currentLetter < wordToType.Length)
		{
			if (settings.disregardHyphens && interChars.hyphens.Contains (wordToType [currentLetter]))
			{
				CompleteLetter (true);
			}
			else if (settings.disregardFullStops && interChars.fullStops.Contains (wordToType [currentLetter]))
			{
				CompleteLetter (true);
			}
			else if (settings.disregardQuestionMarks && interChars.questionMarks.Contains (wordToType [currentLetter]))
			{
				CompleteLetter (true);
			}
			else if (settings.disregardApostrophes && interChars.apostrophes.Contains (wordToType [currentLetter]))
			{
				CompleteLetter (true);
			}
			else if (interChars.otherCharacters.Contains (wordToType [currentLetter]))
			{
				CompleteLetter (true);
			}
			else if (interChars.brackets.Contains (wordToType [currentLetter]))
			{
				CompleteLetter (true);
			}
			else if (interChars.currency.Contains (wordToType [currentLetter]))
			{
				CompleteLetter (true);
			}
			else if (interChars.andSymbols.Contains (wordToType [currentLetter]))
			{
				CompleteLetter (true);
			}
			else if (interChars.numbers.Contains (wordToType [currentLetter]))
			{
				CompleteLetter (true);
			}
		}
	}

	void
	FixedUpdate()
	{
		if (isMoving)
		{
			Move ();
		}
	}

	void
	Move()
	{
		GetComponent<RectTransform> ().anchoredPosition += movement * Time.deltaTime * settings.currentScrollSpeed;
	}

	IncorrectLetter inco { get { return GameObject.Find ("Incorrect Letter").GetComponent<IncorrectLetter> (); } }


	public void
	CheckInput(string input)
	{
		if (isMoving)
		{
			if (input.ToLower () == wordToType [currentLetter].ToString () ||
				input.ToUpper () == wordToType [currentLetter].ToString ())
			{
				CompleteLetter (false);
			}
			else if(input.Contains("’") ||
					input.Contains("'") ||
					input.Contains("‘") ||
					input.Contains("’") ||
					input.Contains("′") ||
					input.Contains("'") ||
					input.Contains("′"))
			{
				if (wordToType [currentLetter].ToString () == "’" ||
					wordToType [currentLetter].ToString () == "'" ||
					wordToType [currentLetter].ToString () == "‘" ||
					wordToType [currentLetter].ToString () == "’" ||
					wordToType [currentLetter].ToString () == "'" ||
					wordToType [currentLetter].ToString () == "'" ||
					wordToType [currentLetter].ToString () == "′")
				{
					CompleteLetter (false);
				}
			}
			else if(!input.Contains("9")
				&& !input.Contains("0")
				&& !input.Contains("1")
				&& !input.Contains("2")
				&& !input.Contains("3")
				&& !input.Contains("4")
				&& !input.Contains("5")
				&& !input.Contains("á")
				&& !input.Contains("ó")
				&& !input.Contains("ú")
				&& !input.Contains("é")
				&& !input.Contains("í"))
			{
				inco.RenderWrongLetter (input);
			}
		}
	}



	void
	CompleteLetter(bool auto)
	{
		if (auto == false)
		{
			GameObject newSFX = Instantiate (letterCompletePrefab);
			newSFX.transform.SetParent (transform);
			newSFX.transform.position = Vector3.zero;

		}

		if(displayLanguage == DisplayLanguage.Target)
			
		{
			IrishOnlyCompleteLetter ();
		}
		else if (displayLanguage == DisplayLanguage.Both)
		{
			BothWordsCompleteLetter ();
		}
		else
		{
			RegularCompleteLetter ();
		}

		if (currentLetter == wordToType.Length)
		{
			CompleteWord (true);
		}
	}

	void
	IrishOnlyCompleteLetter()
	{
		if (currentLetter != wordToType.Length)
		{
			currentLetter++;
		}
			
		UpdateWordAppearance (targetWord);
	}

	void
	BothWordsCompleteLetter()
	{
		if (currentLetter != wordToType.Length)
		{
			currentLetter++;
		}
		UpdateWordAppearance (writtenText);
	}

	void
	RegularCompleteLetter()
	{
		writtenText.text = writtenText.text + wordToType [currentLetter].ToString ();
		if (currentLetter != wordToType.Length)
		{
			currentLetter++;
		}
	}

	void
	UpdateWordAppearance(Text wordy)
	{
		string written = "";
		string notWritten = "";


		for (int i = 0; i < wordToType.Length; i++)
		{
			if (i < currentLetter)
			{
				written += wordToType[i];
			}
			else
				notWritten += wordToType [i];
		}

		wordy.text = ("<color=white>" + written + "</color>" + notWritten);
	}

	public void
	CompleteWord(bool complete)
	{
		if (!complete)
		{
			transform.parent.GetComponent<Lane> ().RegisterMissedWord (word);

			targetWord.color = Color.red;


			writtenText.color = Color.red;
		}
		else
		{
			GameObject.FindObjectOfType<Laneways> ().IncrementCompletePhrases (libraryIndex);

			if (displayLanguage == DisplayLanguage.Target)
			{
				targetWord.color = Color.white;
			}
			if (displayLanguage == DisplayLanguage.Both)
			{
				writtenText.color = Color.white;
			}

			GameObject.FindObjectOfType<PowerupInput>().IncreaseFreezeTime(0.1f * wordToType.Length);

		}

		targetWord.text = targetWord.text.Replace ("<color=white>", "");
		targetWord.text = targetWord.text.Replace ("</color>", "");
		writtenText.text = writtenText.text.Replace ("<color=white>", "");
		writtenText.text = writtenText.text.Replace ("</color>", "");


		transform.parent.GetComponent<Lane> ().Seppuku (gameObject);
		isMoving = false;


		StartCoroutine(FadeAway());
	}

	float waitSeconds = .1f;
	float fadeSeconds = .35f;

	IEnumerator
	FadeAway()
	{
		yield return new WaitForSeconds (waitSeconds);

		float lerpTime = 1f;

		while (lerpTime > 0f)
		{
			targetWord.color = new Color (targetWord.color.r, targetWord.color.g, targetWord.color.b, lerpTime);
			writtenText.color = new Color(writtenText.color.r, writtenText.color.g, writtenText.color.b, lerpTime);
			lerpTime -= Time.deltaTime / fadeSeconds;

			yield return null;
		}

		transform.parent.GetComponent<Lane> ().SeppukuTwo (gameObject);
	}
		

	public void
	Translate()
	{
		if (displayLanguage == DisplayLanguage.Prompt)
		{
			GameObject.FindObjectOfType<Laneways> ().IncrementTranslatedPhrases (libraryIndex);
//			targetWord.text = wordToType;
//			displayLanguage = DisplayLanguage.IrishOnly;
//
//			HideUnderlayText ();
//			UpdateWordAppearance (targetWord);

			displayLanguage = DisplayLanguage.Both;
			UpdateWordAppearance (writtenText);
			writtenText.color = new Color (.35f, .35f, .35f, 1f);
		}
	}

	void
	HideUnderlayText()
	{
		writtenText.text = "";
	}

	public void
	Freeze(bool newStatus)
	{
		if (newStatus == true)
			movement = new Vector2 (0, 0);
		else
			movement = new Vector2 (0, -scrollDistancePerSecond);
	}
}