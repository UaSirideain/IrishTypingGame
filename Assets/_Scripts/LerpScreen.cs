﻿using UnityEngine;
using System.Collections;

public class LerpScreen : MonoBehaviour
{
	[SerializeField] public float duration = 9f;
	[SerializeField] Vector2 open;
	[SerializeField] Vector2 closed;

	[SerializeField] string keyStroke;
	[SerializeField] bool isOpen;
	[SerializeField] bool snapOn;
	[SerializeField] bool closeOtherMenus;

	public void
	Update()
	{
		if(keyStroke != "")
		if (Input.GetKeyDown (keyStroke))
		{
			if (isOpen)
			{
				
				CloseMenu (snapOn);
			}
			else
			{
				
				OpenMenu (snapOn);
			}
		}
	}

	public void
	CloseMenu (bool snap)
	{
		isOpen = false;
		if(snap)GetComponent<RectTransform> ().anchoredPosition = open;
		StartCoroutine (LerpAway (open, closed));
	}

	public void
	OpenMenu(bool snap)
	{
		isOpen = true;

		if (closeOtherMenus)
		foreach (LerpScreen menu in GameObject.FindObjectsOfType<LerpScreen>())
		{
			if(menu.isOpen && menu != this)
			menu.CloseMenu (false);
		}
		if(snap)GetComponent<RectTransform> ().anchoredPosition = closed;
		StartCoroutine (LerpAway (closed, open));
	}

	int currentTicket;

	public IEnumerator
	LerpAway (Vector2 start, Vector2 end)
	{
		float lerpTime = 0f;
		duration = Mathf.Clamp (duration, 0.001f, 100f);

		currentTicket++;
		int myTicket = currentTicket;

		while (lerpTime < 1f && myTicket == currentTicket)
		{
			GetComponent<RectTransform> ().anchoredPosition = Vector2.Lerp (GetComponent<RectTransform> ().anchoredPosition, end, lerpTime);
			lerpTime += Time.deltaTime / duration;

			yield return null;
		}
	}

	public void
	LerpAwayInstant()
	{
		GetComponent<RectTransform>().anchoredPosition = open;
	}
}