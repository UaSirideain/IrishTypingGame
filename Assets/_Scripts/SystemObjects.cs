﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class LibraryFolder : System.Object
{
	public string folderName;
	public List<Library> libraries = new List<Library>();
	public bool fileValidated = false;
}

[System.Serializable]
public class Library : System.Object
{
	public string fileName;
	public string path;
	public string title;
	public string author;
	public string description;
	public List<Phrase> vocabulary = new List<Phrase>();

	public int timesCompleted;//deprecated since version 2

	public bool singularEnabled;
	public PhraseType singularPrompt;

	public bool pluralEnabled;
	public PhraseType pluralPrompt;

	public bool genitiveEnabled;
	public PhraseType genitivePrompt;

	public bool genitivePluralEnabled;
	public PhraseType genitivePluralPrompt;

	public bool dativeEnabled;
	public PhraseType dativePrompt;

	public bool vocativeEnabled;
	public PhraseType vocativePrompt;

	public RatingType rating;
	public int score;
	public int decayedScore;

	public int srsStage;
	public System.DateTime completionDate = new System.DateTime();
	public System.DateTime srsDate = new System.DateTime();


	public void
	Initialise()
	{
		if (srsStage < 7)
		{
			DecayRating1(new System.TimeSpan(1, 0, 0, 0));
		}
		else if (srsStage < 11)
		{
			DecayRating1(new System.TimeSpan(7, 0, 0, 0));
		}
		else if (srsStage < 23)
		{
			DecayRating1(new System.TimeSpan(28, 0, 0, 0));
		}
	}

	public void
	DecayRating1(System.TimeSpan timeSpan)
	{
		System.DateTime currentDate = System.DateTime.Now;
		System.TimeSpan duration = currentDate.Subtract (completionDate);


		int whiler = 10;
		int decayingScore = score;

		while (whiler > 0)
		{
			if (duration > timeSpan)
			{
				duration -= timeSpan;
				decayingScore -= 10;
			}
			else
			{
				break;
			}

			whiler--;
		}

		decayedScore = decayingScore;
		DecayRating ();
	}


	public void
	DecayRating()
	{
		if (decayedScore > 90)
			rating = RatingType.SeanFhainne;
		else if (decayedScore > 75)
			rating = RatingType.FainneOir;
		else if (decayedScore > 55)
			rating = RatingType.FainneAirgid;
		else if (decayedScore > 10)
			rating = RatingType.CuplaFocal;
		else
			rating = RatingType.None;
	}


	public void
	SetPhraseType(ref PhraseType promptType, string newValue)
	{
		promptType = (PhraseType)System.Enum.Parse (typeof(PhraseType), newValue);
	}

	public void
	ResolveMissingCaseErrors (ref PhraseType promptType)
	{
		promptType = (PhraseType)GetNextValidCase (promptType);
	}

	int
	GetNextValidCase(PhraseType promptType)
	{
		int currentCase = (int)promptType;
		bool currentCaseValid = false;

		Phrase caseFinder = CheckForAllCases ();

		int newCurrentCase = currentCase;
		for(int i = currentCase; i < currentCase + 9; i++)
		{
			if (newCurrentCase == 0 && caseFinder.prompt != null)
			{
				currentCaseValid = true;
				return newCurrentCase;
			}
			else if (newCurrentCase == 1 && caseFinder.target != null)
			{
				currentCaseValid = true;
				return newCurrentCase;
			}
			else if (newCurrentCase == 2 && caseFinder.targetPlural != null)
			{
				currentCaseValid = true;
				return newCurrentCase;
			}
			else if (newCurrentCase == 3 && caseFinder.targetGenitive != null)
			{
				currentCaseValid = true;
				return newCurrentCase;
			}
			else if (newCurrentCase == 4 && caseFinder.targetGenitivePlural != null)
			{
				currentCaseValid = true;
				return newCurrentCase;
			}
			else if (newCurrentCase == 5 && caseFinder.targetDative != null)
			{
				currentCaseValid = true;
				return newCurrentCase;
			}
			else if (newCurrentCase == 6 && caseFinder.targetVocative != null)
			{
				currentCaseValid = true;
				return newCurrentCase;
			}

			if (newCurrentCase++ > 6)
			{
				newCurrentCase = 0;
			}
		}
		return newCurrentCase;
	}

	public Phrase
	CheckForAllCases()
	{
		Phrase platonicPhrase = new Phrase();

		foreach (Phrase phrase in vocabulary)
		{
			//			if (phrase.prompt != null)
			//			{
			//				platonicPhrase.prompt = "phrase";
			//			}
			if (phrase.target != null)
			{
				platonicPhrase.target = "phrase";
			}
			if (phrase.targetPlural != null)
			{
				platonicPhrase.targetPlural = "phrase";
			}
			if (phrase.targetGenitive != null)
			{
				platonicPhrase.targetGenitive = "phrase";
			}
			if (phrase.targetGenitivePlural != null)
			{
				platonicPhrase.targetGenitivePlural = "phrase";
			}
			if (phrase.targetDative != null)
			{
				platonicPhrase.targetDative = "phrase";
			}
			if (phrase.targetVocative != null)
			{
				platonicPhrase.targetVocative = "phrase";
			}
			if (phrase.targetOnly == false)//I'm pretty sure ALL phrases have to be target only if even one of them is. This should REALLY be fucking changed.
			{
				platonicPhrase.prompt = "prompt";
			}
		}

		return platonicPhrase;
	}

	public int
	CasesInstalled()
	{
		Phrase platonicPhrase = CheckForAllCases ();

		int casesInstalled = 0;

		if (platonicPhrase.target == "phrase")
			casesInstalled++;
		if (platonicPhrase.targetPlural == "phrase")
			casesInstalled++;
		if (platonicPhrase.targetGenitive == "phrase")
			casesInstalled++;
		if (platonicPhrase.targetGenitivePlural == "phrase")
			casesInstalled++;
		if (platonicPhrase.targetDative == "phrase")
			casesInstalled++;
		if (platonicPhrase.targetVocative == "phrase")
			casesInstalled++;

		return casesInstalled;
	}

	public int
	ActiveCasesCount()
	{
		int activeCases = 0;

		if (singularEnabled)
			activeCases++;
		if (pluralEnabled)
			activeCases++;
		if (genitiveEnabled)
			activeCases++;
		if (genitivePluralEnabled)
			activeCases++;
		if (dativeEnabled)
			activeCases++;
		if (vocativeEnabled)
			activeCases++;

		return activeCases;
	}

	public void
	NewRating(float newScore, RatingType newRating)
	{
		bool increaseSRS = false;

		if (newRating == RatingType.SeanFhainne)
		{
			rating = RatingType.SeanFhainne;
			increaseSRS = true;
			completionDate = System.DateTime.Now;
		}
		else if (newRating == RatingType.FainneOir && rating != RatingType.SeanFhainne)
		{
			rating = RatingType.FainneOir;
			increaseSRS = true;
			completionDate = System.DateTime.Now;
		}
		else if (newRating == RatingType.FainneAirgid && rating != RatingType.SeanFhainne && rating != RatingType.FainneOir)
		{
			rating = RatingType.FainneAirgid;
			increaseSRS = true;
			completionDate = System.DateTime.Now;
		}
		else if (newRating == RatingType.CuplaFocal && (rating == RatingType.None || rating == RatingType.CuplaFocal))
		{
			rating = RatingType.CuplaFocal;
			increaseSRS = true;
			completionDate = System.DateTime.Now;
		}

		if (newScore > decayedScore)
			score = (int)newScore;

		if (increaseSRS)
		{
			if (srsStage == 0)
			{
				srsStage++;
				srsDate = System.DateTime.Now;
			}
			else if (srsStage < 7)
			{
				if (System.DateTime.Now - srsDate >= new System.TimeSpan(0, 20, 0, 0))
				{
					srsStage++;
					srsDate = System.DateTime.Now;
				}
			}
			else if (srsStage < 11)
			{
				if (System.DateTime.Now - srsDate >= new System.TimeSpan(5, 0, 0, 0))
				{
					srsStage++;
					srsDate = System.DateTime.Now;
				}
			}
			else if (srsStage < 23)
			{
				if (System.DateTime.Now - srsDate >= new System.TimeSpan(23, 0, 0, 0))
				{
					srsStage++;
					srsDate = System.DateTime.Now;
				}
			}
		}

		if (Application.platform != RuntimePlatform.WebGLPlayer)
		{
			LibrarySerialiser.WriteSerialisedData (this);
		}
	}
		

	public List<Preview>
	GetActivePhrases()
	{
		List<Preview> previews = new List<Preview> ();

		if (singularEnabled)
		{
			foreach (Phrase phrase in vocabulary)
			{
				if (phrase.target != null)
				{
					Preview preview = new Preview ();
					preview.topText = phrase.GetPrompt (singularPrompt, PhraseType.Singular);
					preview.bottomText = phrase.target;
					previews.Add (preview);
				}
			}
		}
		if (pluralEnabled)
		{
			foreach (Phrase phrase in vocabulary)
			{
				if (phrase.targetPlural != null)
				{
					Preview preview = new Preview ();
					preview.topText = phrase.GetPrompt (pluralPrompt, PhraseType.Plural);
					preview.bottomText = phrase.targetPlural;
					previews.Add (preview);
				}
			}
		}
		if (genitiveEnabled)
		{
			foreach (Phrase phrase in vocabulary)
			{
				if (phrase.targetGenitive != null)
				{
					Preview preview = new Preview ();
					preview.topText = phrase.GetPrompt (genitivePrompt, PhraseType.Genitive);
					preview.bottomText = phrase.targetGenitive;
					previews.Add (preview);
				}
			}
		}
		if (genitivePluralEnabled)
		{
			foreach (Phrase phrase in vocabulary)
			{
				if (phrase.targetGenitivePlural != null)
				{
					Preview preview = new Preview ();
					preview.topText = phrase.GetPrompt (genitivePluralPrompt, PhraseType.GenitivePlural);
					preview.bottomText = phrase.targetGenitivePlural;
					previews.Add (preview);
				}
			}
		}
		if (vocativeEnabled)
		{
			foreach (Phrase phrase in vocabulary)
			{
				if (phrase.targetVocative != null)
				{
					Preview preview = new Preview ();
					preview.topText = phrase.GetPrompt (vocativePrompt, PhraseType.Vocative);
					preview.bottomText = phrase.targetVocative;
					previews.Add (preview);
				}
			}
		}
		if (dativeEnabled)
		{
			foreach (Phrase phrase in vocabulary)
			{
				if (phrase.targetDative != null)
				{
					Preview preview = new Preview ();
					preview.topText = phrase.GetPrompt (dativePrompt, PhraseType.Dative);
					preview.bottomText = phrase.targetDative;
					previews.Add (preview);
				}
			}
		}
		return previews;
	}

}

public class Preview : System.Object
{
	public string topText;
	public string bottomText;
}

public enum RatingType{Unscored, None, CuplaFocal, FainneAirgid, FainneOir, SeanFhainne}

[System.Serializable]
public class Phrase : System.Object
{
	public string prompt;
	public string promptPlural;
	public string promptGenitive;
	public string promptGenitivePlural;
	public string promptVocative;
	public string promptDative;

	public string target;
	public string targetGenitive;
	public string targetPlural;
	public string targetGenitivePlural;
	public string targetVocative;
	public string targetDative;

	public bool targetOnly;


	public string
	GetPrompt(PhraseType promptStyle, PhraseType targetStyle)
	{
		string newPrompt = "";

		switch(promptStyle)
		{
		case PhraseType.English:
			//I need to know what type I am as well if I want to get a case specific english phrase
			switch (targetStyle)
			{
			case PhraseType.Singular:
				newPrompt = prompt;
				break;

			case PhraseType.Plural:
				newPrompt = promptPlural;
				break;

			case PhraseType.Genitive:
				newPrompt = promptGenitive;
				break;

			case PhraseType.GenitivePlural:
				newPrompt = promptGenitivePlural;
				break;

			case PhraseType.Dative:
				newPrompt = promptDative;
				break;

			case PhraseType.Vocative:
				newPrompt = promptVocative;
				break;
			}
			break;

		case PhraseType.Singular:
			newPrompt = target;
			break;

		case PhraseType.Plural:
			newPrompt = targetPlural;
			break;

		case PhraseType.Genitive:
			newPrompt = targetGenitive;
			break;

		case PhraseType.GenitivePlural:
			newPrompt = targetGenitivePlural;
			break;

		case PhraseType.Vocative:
			newPrompt = targetVocative;
			break;

		case PhraseType.Dative:
			newPrompt = targetDative;
			break;
		}

		if (newPrompt == null)
			newPrompt = prompt;

		return newPrompt;
	}
}