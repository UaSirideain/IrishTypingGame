﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas))]
[ExecuteInEditMode]
public class LetterBox : MonoBehaviour 
{
	// UI camera
	public Camera targetCamera;

	// resolution
	public Vector2 screenSize = new Vector2(800, 600);

	private float aspect;
	private Canvas targetCanvos;

	public void Start()
	{
		UpdateResolution();
	}

	#if UNITY_EDITOR

	void Update () 
	{
		if(! UnityEditor.EditorApplication.isPlaying)
		{
			UpdateResolution();
		}
	}

	#endif


	public void
	CallUpdate()
	{
		Invoke ("UpdateResolution", 1f);
		Invoke ("UpdateResolution", 2f);
	}


	void OnValidate()
	{
		UpdateResolution();
	}

	private Canvas TargetCanvas
	{
		get
		{
			if( targetCanvos == null )
				targetCanvos = GetComponent<Canvas>();
			return targetCanvos;
		}
	}

	float GetAspect()
	{
		return (float)Screen.height / Screen.width;
	}

	void UpdateResolution()
	{
		if( targetCamera == null || TargetCanvas == null )
		{
			Debug.LogWarning("camera or canvous is not reference");
			return;
		}

		// if same aspect as cash, skip.
		float currentAspect = GetAspect();
		if( currentAspect == aspect  )
		{
			return;
		}
		aspect = currentAspect;

		// update resolution

		TargetCanvas.renderMode = RenderMode.WorldSpace;
		TargetCanvas.worldCamera = targetCamera;
		TargetCanvas.transform.localScale = new Vector3(1,1,1); 

		var rectT = TargetCanvas.GetComponent<RectTransform>();
		rectT.sizeDelta = screenSize;

		var referenceScreenRate = screenSize.y / screenSize.x;
		var physicalScreenRate = aspect;
		var diffScreenRate = physicalScreenRate / referenceScreenRate;
		var heightHalf = screenSize.y * 0.5f;

		targetCamera.orthographicSize = 
			(diffScreenRate > 1) ? heightHalf * diffScreenRate : heightHalf;
	}


	void CreateUICamera()
	{
		transform.parent = null;
		DestroyImmediate ( GameObject.Find("UI Camera"));

		var uiCamera = new GameObject("UI Camera");
		uiCamera.transform.localPosition = 
			transform.position + transform.forward * -10;

		var cam = uiCamera.AddComponent<Camera>();
		cam.cullingMask = 1 << LayerMask.NameToLayer("UI");
		cam.clearFlags = CameraClearFlags.Depth;
		cam.orthographic = true;
		cam.depth = 99;

		targetCamera = cam;

		transform.parent = targetCamera.transform;
	}

	[ContextMenu("set up")]
	void Init()
	{
		targetCanvos = GetComponent<Canvas>();

		if( targetCamera == null )
			CreateUICamera();

		UpdateResolution();
	}
}