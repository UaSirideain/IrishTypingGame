﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ReferenceVariables : MonoBehaviour
{
	public static List<Color> laneColours = new List<Color>();

	void
	Awake()
	{
		laneColours.Add (new Color (.84f, .36f, .36f, 1f));
		laneColours.Add (new Color (.49f, .84f, .58f, 1f));
		laneColours.Add (new Color (.82f, .51f, .72f, 1f));
		laneColours.Add (new Color (.46f, .70f, .84f, 1f));
		laneColours.Add (new Color (.89f, .62f, .37f, 1f));
	}
}