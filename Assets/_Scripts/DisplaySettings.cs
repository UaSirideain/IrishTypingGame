﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class DisplaySettings : MonoBehaviour
{
	[SerializeField] GameObject fullscreenButtonTick;
	[SerializeField] Text currentResolutionText;
	[SerializeField] LetterBox letterbox;

	[SerializeField] List<Vector2> supportedResolutions = new List<Vector2>();
	public int currentResolution;

	int resolutionX;
	int resolutionY;
	public bool fullscreen;

	void
	Start()
	{
		fullscreen = Screen.fullScreen;
		UpdateResolutionUI ();
	}

	public void
	ResolutionButton()
	{
		currentResolution++;

		if (currentResolution >= supportedResolutions.Count)
			currentResolution = 0;

		if (fullscreen)
		{
			bool supported = false;

			foreach (Resolution res in Screen.resolutions)
			{
				if (res.width >= supportedResolutions [currentResolution].x)
				{
					supported = true;
				}
				if (res.height >= supportedResolutions [currentResolution].y)
				{
					supported = true;
				}
			}

			if (supported == false)
			{
				currentResolution = 0;
			}
		}
		else
		{
			if (supportedResolutions [currentResolution].x > Screen.currentResolution.width || supportedResolutions [currentResolution].y > Screen.currentResolution.height)
				currentResolution = 0;
		}


		UpdateResolution ();
	}

	public void
	FullscreenButton()
	{
		fullscreen = !fullscreen;

		UpdateResolution ();
	}

	void
	UpdateResolution()
	{
		Screen.SetResolution ((int)supportedResolutions [currentResolution].x, (int)supportedResolutions [currentResolution].y, fullscreen);
		Invoke("UpdateResolutionUI", .3f);
	}

	void
	UpdateResolutionUI()
	{
		currentResolutionText.text = Screen.width.ToString () + " x " + Screen.height.ToString ();
		fullscreenButtonTick.SetActive (Screen.fullScreen);
		letterbox.CallUpdate();
	}

	public void
	SetCurrentResolution(string i)
	{
		currentResolution = int.Parse(i);
		if (currentResolution >= supportedResolutions.Count)
		{
			currentResolution = supportedResolutions.Count - 1;
		}
	}

	public void
	SetFullscreen(string full)
	{
		fullscreen = bool.Parse(full);
	}
}