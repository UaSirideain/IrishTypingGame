﻿using UnityEngine;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class LibraryParser : MonoBehaviour
{
	public static List<Library>
	GetInstalledLibraries()
	{
		List<Library> installedLibraries = new List<Library> ();

		string librariesPath = "";

		if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.LinuxPlayer)
		{
			librariesPath = "Libraries";
		}
		else if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
		{
			librariesPath = Application.dataPath + "/Resources/Libraries";
		}
			
		if (Application.platform != RuntimePlatform.WebGLPlayer)
		{
			foreach (string file in Directory.GetFiles(librariesPath))
			{
				if (file.Contains (".meta") == false)
					installedLibraries.Add (ParseLibrary(file));
			}
		}
		else
		{
			foreach (UnityEngine.Object file in Resources.LoadAll("Libraries"))
			{
				if (file.name.Contains (".meta") == false)
					installedLibraries.Add (ParseLibrary(file.name));
				
			}
		}

		return installedLibraries;
	}


	public static List<LibraryFolder>
	GetInstalledLibraryFolders()
	{
		List<LibraryFolder> installedLibraryFolders = new List<LibraryFolder> ();

		string librariesPath = "";

		if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.LinuxPlayer)
		{
			librariesPath = "Libraries";
		}
		else if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
		{
			librariesPath = Application.dataPath + "/Resources/Libraries";
		}

		for (var i = 0; i < Directory.GetDirectories(librariesPath).Length; i++)	
		{
			LibraryFolder newLibFolder = new LibraryFolder();



			string folderPath = Directory.GetDirectories (librariesPath) [i].Replace ("Libraries", "");
			newLibFolder.folderName = folderPath.Replace("\\", "");

			if (Application.platform == RuntimePlatform.WebGLPlayer)
			{
				//fill in the blanks
			}
			else
			{
				foreach (string file in Directory.GetFiles(librariesPath + folderPath))
				{
					if(file.Contains(".meta") == false)
					{
						newLibFolder.libraries.Add (ParseLibrary(file));
					}
				}
			}

			if(newLibFolder.libraries.Count > 0)
			installedLibraryFolders.Add (newLibFolder);
		}

		return installedLibraryFolders;
	}

	public static Library
	ParseLibrary(string libraryName)
	{
		Library lib = new Library ();

		if (Application.platform != RuntimePlatform.WebGLPlayer)
		{
			lib = ParseLibraryIO (libraryName);
		}
		else
		{
			lib = ParseLibraryGL (libraryName);
		}

		return lib;
	}

//	static string
//	OpenFolder(string libraryName)
//	{
//		string newLibName = libraryName;
//		while(newLibName.Contains("
//	}

	public static Library
	ParseLibraryIO (string libraryName)
	{
		if (File.Exists (libraryName))
		{
			string rawText = File.ReadAllText (libraryName);
			List<string> splitText = new List<string> (Regex.Split (rawText, Environment.NewLine));

			Library parsedLibrary = LoadWords(splitText);

			parsedLibrary.path = libraryName;

			parsedLibrary.timesCompleted = GameObject.FindObjectOfType<UserDataSerialiser>().LoadLibraryMetaData (libraryName);

			return parsedLibrary;
		}

		return new Library();
	}

	public static Library
	ParseLibraryGL (string libraryName)
	{
		TextAsset file = (TextAsset)Resources.Load ("Libraries/" + libraryName, typeof(TextAsset));

		if (file != null)
		{
			string rawText = file.text;
			List<string> splitText = new List<string> (Regex.Split (rawText, Environment.NewLine));
			Library parsedLibrary = LoadWords(splitText);
			parsedLibrary.path = libraryName;
			//parsedLibrary.timesCompleted = GameObject.FindObjectOfType<SaveData>().LoadLibraryMetaData (libraryName);

			return parsedLibrary;
		}

		return new Library();
	}

	public static Library
	LoadWords(List<string> lines)
	{
		if (lines[0].Contains ("Parser Version 1"))
		{
			return LoadWordsPV1 (lines);
		}
		GameObject.FindObjectOfType<ErrorMessage> ().ChangeText ("LP1");//could not read parser version

		return null;
	}


	public static Library
	LoadWordsPV1(List<string> lines)
	{
		Library library = new Library ();

		string format = "";
		foreach (string line in lines)
		{
			string pLine = line.Replace ("\r", "").Replace ("\n", "");

			string prompt = "";
			string target = "";
			bool targetOnly = false;

			bool lineIsPhrase = false;

			//GameObject.FindObjectOfType<DebugText> ().ChangeText (pLine);


			if (pLine == "" || pLine [0] == "-" [0])
			{
				//ignore all blank lines and lines prefixed with a hyphen
			}
			else if (pLine.Contains ("Name:"))
			{
				library.title = pLine.Replace ("Name: ", "");
				library.title = library.title.Replace ("Name:", "");
				library.title = library.title.Replace ("Name", "");

				//GameObject.FindObjectOfType<DebugText> ().ChangeText (pLine);
				//GameObject.FindObjectOfType<DebugText> ().ChangeText (library.title);
			}
			else if (pLine.Contains ("Author: "))
			{
				library.author = pLine.Replace ("Author: ", "");
				library.author = library.author.Replace ("Author:", "");
				library.author = library.author.Replace ("Author", "");
			}
			else if (pLine.Contains ("Format: "))
			{
				format = pLine.Replace ("Format: ", "");
				format = format.Replace ("Format:", "");
				format = format.Replace ("Format", "");
			}

			else if (format.Contains("english/irish") || format.Contains("English/Irish"))
			{
				prompt = pLine.Split ("/".ToCharArray (), 2) [0];
				target = pLine.Split ("/".ToCharArray (), 2) [1];

				targetOnly = false;
				lineIsPhrase = true;
			}
			else if (format.Contains("irish/english") || format.Contains("Irish/English"))
			{
				prompt = pLine.Split ("/".ToCharArray (), 2) [1];
				target = pLine.Split ("/".ToCharArray (), 2) [0];

				targetOnly = false;
				lineIsPhrase = true;
			}
			else if (format.Contains("irish") || format.Contains("Irish"))
			{
				prompt = pLine;
				target = pLine;

				targetOnly = true;
				lineIsPhrase = true;
			}
			else if (format.Contains("term/definition") || format.Contains("Term/Definition"))
			{
				prompt = pLine.Split ("/".ToCharArray (), 2) [0];
				target = pLine.Split ("/".ToCharArray (), 2) [1];

				lineIsPhrase = true;
			}

			if (format != "" && lineIsPhrase)
				GetDeclensions (prompt, target, ref library, targetOnly);

		}

		return library;
	}


	static void
	GetDeclensions(string native, string target, ref Library library, bool targetOnly)
	{
		Phrase newPhrase = new Phrase ();

		//newPhrase.prompt = native.Trim();

		newPhrase.targetOnly = targetOnly;

		string currentCase = "";
		string currentWord = "";

		bool caseOpen = false;

		for (int i = 0; i < native.Length; i++)
		{
			if(native[i] == "["[0] || i == native.Length - 1)//in other words, if we just parsed the last letter of a phrase
			{
				if (i == native.Length - 1)
				{
					currentWord += native [i];
				}
				currentWord = currentWord.Trim ();
				if(currentCase.Contains("gp"))
				{
					newPhrase.promptGenitivePlural = currentWord;
				}
				if(currentCase.Contains("pl"))
				{
					newPhrase.promptPlural = currentWord;
				}
				if(currentCase.Contains("gn"))
				{
					newPhrase.promptGenitive = currentWord;
				}
				if(currentCase.Contains("d"))
				{
					newPhrase.promptDative = currentWord;
				}
				if(currentCase.Contains("v"))
				{
					newPhrase.promptVocative = currentWord;
				}
				if(currentCase.Contains("nom") || currentCase == "")
				{
					newPhrase.prompt = currentWord;
				}

				currentCase = "";
				currentWord = "";
				caseOpen = true;
			}
			else if(native[i] == "]"[0])
			{
				caseOpen = false;
			}
			else if (caseOpen)
			{
				currentCase += native [i];
			}
			else if (!caseOpen)
			{
				currentWord += native [i];
			}
		}

		currentCase = "";
		currentWord = "";

		caseOpen = false;

		for (int i = 0; i < target.Length; i++)
		{
			if(target[i] == "["[0] || i == target.Length - 1)//in other words, if we just parsed the last letter of a phrase
			{
				if (i == target.Length - 1)
				{
					currentWord += target [i];
				}
				currentWord = currentWord.Trim ();
				if(currentCase.Contains("gp"))
				{
					newPhrase.targetGenitivePlural = currentWord;
				}
				if(currentCase.Contains("pl"))
				{
					newPhrase.targetPlural = currentWord;
				}
				if(currentCase.Contains("gn"))
				{
					newPhrase.targetGenitive = currentWord;
				}
				if(currentCase.Contains("d"))
				{
					newPhrase.targetDative = currentWord;
				}
				if(currentCase.Contains("v"))
				{
					newPhrase.targetVocative = currentWord;
				}
				if(currentCase.Contains("nom") || currentCase == "")
				{
					if(currentWord.Trim() != "")
					newPhrase.target = currentWord;
				}

				currentCase = "";
				currentWord = "";
				caseOpen = true;
			}
			else if(target[i] == "]"[0])
			{
				caseOpen = false;
			}
			else if (caseOpen)
			{
				currentCase += target [i];
			}
			else if (!caseOpen)
			{
				currentWord += target [i];
			}
		}

		AssignEmptyPromptCases (ref newPhrase);

		library.vocabulary.Add (newPhrase);
	}

	static void
	AssignEmptyPromptCases(ref Phrase phrase)
	{
		string mainPrompt = "";

		if (phrase.prompt != null)
			mainPrompt = phrase.prompt;
		else if (phrase.promptPlural != null)
			mainPrompt = phrase.promptPlural;
		else if (phrase.promptGenitive != null)
			mainPrompt = phrase.promptGenitive;
		else if (phrase.promptGenitivePlural != null)
			mainPrompt = phrase.promptGenitivePlural;
		else if (phrase.promptDative != null)
			mainPrompt = phrase.promptDative;
		else if (phrase.promptVocative != null)
			mainPrompt = phrase.promptVocative;

		if (phrase.prompt == null)
			phrase.prompt = mainPrompt + " (singular)";
		if (phrase.promptPlural == null)
			phrase.promptPlural = mainPrompt + " (plural)";
		if (phrase.promptGenitive == null)
			phrase.promptGenitive = mainPrompt + " (genitive)";
		if (phrase.promptGenitivePlural == null)
			phrase.promptGenitivePlural = mainPrompt + " (genitive plural)";
		if (phrase.promptDative == null)
			phrase.promptDative = mainPrompt + " (dative)";
		if (phrase.promptVocative == null)
			phrase.promptVocative = mainPrompt + " (vocative)";
	}
}