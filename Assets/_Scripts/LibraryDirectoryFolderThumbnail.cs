﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LibraryDirectoryFolderThumbnail : MonoBehaviour
{
	List<Library> libraries;
	LibraryDirectory myFolder;
	[SerializeField] Text title;

	[Header("Prefabs")]
	[SerializeField] LibraryDirectory folderPagePrefab;
	//when you click on it, it throws up a page with it's own libraries

	public bool pointerIn {get;set;}

	public void
	Initialise(LibraryFolder newFolder)
	{
		libraries = newFolder.libraries;

		title.text = newFolder.folderName;

		myFolder = Instantiate (folderPagePrefab);
		myFolder.DisplayLibraries (new List<LibraryFolder> (), libraries);
		myFolder.transform.GetChild (5).GetComponent<Text> ().text = newFolder.folderName;

		//myFolder.gameObject.SetActive (false);
		myFolder.transform.SetParent (transform, true);
		myFolder.GetComponent<RectTransform> ().localPosition = new Vector3 (30000, 30000, -30000);

		//foreach folderPage, create a folder page
	}

	void
	Update()
	{
		if (pointerIn == true && Input.GetMouseButtonDown(0))
		{
			myFolder.gameObject.SetActive (true);
			myFolder.OpenPageSnappy (0);
			myFolder.transform.SetParent (GameObject.Find ("Library Directory").transform);
			myFolder.transform.localPosition = new Vector3 (0, 0, 0);
		}
	}
}