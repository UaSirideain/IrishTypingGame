﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections.Generic;

public class LibrarySerialiser : MonoBehaviour
{
	public static void
	LoadSerialisedData(ref Library library)
	{
		List<string> data = LoadDataFromFile(library);
		foreach (string line in data)
		{
			if (line.Contains ("TimesCompleted="))
				library.timesCompleted = int.Parse (line.Replace ("TimesCompleted=", ""));
			else if (line.Contains ("HighestRating="))
				library.rating = (RatingType)System.Enum.Parse (typeof(RatingType), line.Replace ("HighestRating=", ""));
			else if (line.Contains ("SingularEnabled="))
				library.singularEnabled = bool.Parse (line.Replace ("SingularEnabled=", ""));
			else if (line.Contains ("SingularPrompt="))
				library.SetPhraseType (ref library.singularPrompt, line.Replace ("SingularPrompt=", ""));
			else if (line.Contains ("PluralEnabled="))
				library.pluralEnabled = bool.Parse (line.Replace ("PluralEnabled=", ""));
			else if (line.Contains ("PluralPrompt="))
				library.SetPhraseType (ref library.pluralPrompt, line.Replace ("PluralPrompt=", ""));
			else if (line.Contains ("GenitiveEnabled="))
				library.genitiveEnabled = bool.Parse (line.Replace ("GenitiveEnabled=", ""));
			else if (line.Contains ("GenitivePrompt="))
				library.SetPhraseType (ref library.genitivePrompt, line.Replace ("GenitivePrompt=", ""));
			else if (line.Contains ("GenPlurEnabled="))
				library.genitivePluralEnabled = bool.Parse (line.Replace ("GenPlurEnabled=", ""));
			else if (line.Contains ("GenPlurPrompt="))
				library.SetPhraseType (ref library.genitivePluralPrompt, line.Replace ("GenPlurPrompt=", ""));
			else if (line.Contains ("DativeEnabled="))
				library.dativeEnabled = bool.Parse (line.Replace ("DativeEnabled=", ""));
			else if (line.Contains ("DativePrompt="))
				library.SetPhraseType (ref library.dativePrompt, line.Replace ("DativePrompt=", ""));
			else if (line.Contains ("VocativeEnabled="))
				library.vocativeEnabled = bool.Parse (line.Replace ("VocativeEnabled=", ""));
			else if (line.Contains ("VocativePrompt="))
				library.SetPhraseType (ref library.vocativePrompt, line.Replace ("VocativePrompt=", ""));
			else if(line.Contains("Score="))
				library.score = int.Parse(line.Replace("Score=", ""));
			else if (line.Contains("SRSRank="))
				library.srsStage = int.Parse(line.Replace("SRSRank=", ""));
			else if(line.Contains("CompletionDate="))
				library.completionDate = System.DateTime.Parse(line.Replace("CompletionDate=", ""));
			else if(line.Contains("SRSDate="))
				library.srsDate = System.DateTime.Parse(line.Replace("SRSDate=", ""));
		}
		library.Initialise ();
	}

	public static void
	WriteSerialisedData(Library library)
	{
		List<string> newData = new List<string> ();

		newData.Add ("TimesCompleted=" + library.timesCompleted.ToString());
		newData.Add ("HighestRating=" + library.rating.ToString());
		newData.Add ("SingularEnabled=" + library.singularEnabled.ToString ());
		newData.Add ("SingularPrompt=" + library.singularPrompt.ToString ());
		newData.Add ("PluralEnabled=" + library.pluralEnabled.ToString ());
		newData.Add ("PluralPrompt=" + library.pluralPrompt.ToString ());
		newData.Add ("GenitiveEnabled=" + library.genitiveEnabled.ToString ());
		newData.Add ("GenitivePrompt=" + library.genitivePrompt.ToString ());
		newData.Add ("GenPlurEnabled=" + library.genitivePluralEnabled.ToString ());
		newData.Add ("GenPlurPrompt=" + library.genitivePluralPrompt.ToString ());
		newData.Add ("DativeEnabled=" + library.dativeEnabled.ToString ());
		newData.Add ("DativePrompt=" + library.dativePrompt.ToString ());
		newData.Add ("VocativeEnabled=" + library.vocativeEnabled.ToString ());
		newData.Add ("VocativePrompt=" + library.vocativePrompt.ToString ());
		newData.Add ("Score=" + library.score.ToString ());
		newData.Add ("SRSRank=" + library.srsStage.ToString ());
		newData.Add ("CompletionDate=" + library.completionDate.ToString());
		newData.Add ("SRSDate=" + library.srsDate.ToString());

		//SaveDataToFile(newData, path);
		string libraryPath = library.path.Replace("Libraries", "");
		File.WriteAllLines ("PlayerData" + libraryPath, newData.ToArray());
	}

	static List<string>
	LoadDataFromFile(Library library)
	{
		FileStream file;

		string libraryPath = library.path.Replace("Libraries", "");

		if (File.Exists ("PlayerData" + libraryPath))
		{
			file = File.OpenRead ("PlayerData" + libraryPath);
		}
		else
		{
//			if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
			//{
				//Directory.CreateDirectory ("PlayerData");
			//}

			string newLibraryPath = libraryPath.Substring (1);
			string newFolderPath = "";
			if (newLibraryPath.Contains ("\\"))
			{
				newFolderPath = newLibraryPath.Split ("\\".ToCharArray ()) [0];
				//newLibraryPath = newLibraryPath.Replace (newFolderPath, "");
				newLibraryPath = newLibraryPath.Split("\\".ToCharArray())[1];

				Directory.CreateDirectory ("PlayerData\\" + newFolderPath);
			}
			file = File.Create ("PlayerData\\" + newFolderPath + "\\" + newLibraryPath);
		}

		file.Close ();

		string rawData = File.ReadAllText ("PlayerData" + libraryPath);

		List<string> data = new List<string>(Regex.Split(rawData, Environment.NewLine));

		return data;
	}
}