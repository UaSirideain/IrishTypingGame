﻿using UnityEngine;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class UserDataSerialiser : MonoBehaviour
{
	[SerializeField] string saveFileName = "";
	[SerializeField] string librariesPath = "";
	string saveFilePath = "";
	[SerializeField] int maxLanes = 5;
	[SerializeField] GameplaySettings settings;
	[SerializeField] EngineSettings engineSettings;
	[SerializeField] DisplaySettings displaySettings;
	[SerializeField] LibraryDirectory libraryDirectory;

	void
	Awake()
	{
		if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.LinuxPlayer)
		{
			saveFileName = "SaveData.txt";
			librariesPath = "Libraries";
		}
		else if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
		{
			saveFilePath = Environment.GetFolderPath (Environment.SpecialFolder.Personal) + "/Library/Application Support/IrishTypingGame";
			saveFileName = saveFilePath + "/SaveData.txt";
			librariesPath = Application.dataPath + "/Resources/Libraries";
		}
	}

	void
	Start ()
	{
		if (Application.platform != RuntimePlatform.WebGLPlayer)
		{
			LoadSaveDataIntoScene ();
		}
	}


	public void
	LoadSaveDataIntoScene()
	{
		List<string> saveData = ConvertSaveToString ();

		foreach (string line in saveData)
		{
			string pLine = line.Replace ("\r", "").Replace ("\n", "");

			for (int i = 1; i != maxLanes + 1; i++)
			{
				if (pLine.Contains ("DefaultLibrary" + i) && !pLine.EndsWith ("="))
				{
					//print (pLine.Split("=".ToCharArray(), 2) [1]);
					//settings.AddLibrary (LibraryParser.ParseLibrary (pLine.Split ("=".ToCharArray (), 2) [1]));
					libraryDirectory.FindAndAdd(LibraryParser.ParseLibrary (pLine.Split ("=".ToCharArray (), 2) [1]));
				}
			}

			if (pLine.Contains ("DisregardHyphens"))
			{
				settings.SetHyphens (pLine.Replace ("DisregardHyphens=", ""));
			}
			else if (pLine.Contains ("DisregardCapitals"))
			{
				settings.SetCapitals (pLine.Replace ("DisregardCapitals=", ""));
			}
			else if (pLine.Contains ("DisregardApostrophes"))
			{
				settings.SetApostrophes (pLine.Replace ("DisregardApostrophes=", ""));
			}
			else if (pLine.Contains ("DisregardQuestionMarks"))
			{
				settings.SetQuestionMarks (pLine.Replace ("DisregardQuestionMarks=", ""));
			}
			else if (pLine.Contains ("DisregardFullStops"))
			{
				settings.SetFullStops (pLine.Replace ("DisregardFullStops=", ""));
			}
			else if (pLine.Contains ("PowerupTranslate"))
			{
				settings.SetTranslatePowerupSetting (pLine.Split ("=".ToCharArray (), 2) [1]);
			}
			else if (pLine.Contains ("PowerupFreeze"))
			{
				settings.SetFreezePowerupSetting (pLine.Split ("=".ToCharArray (), 2) [1]);
			}
			else if (pLine.Contains ("Frequency"))
			{
				settings.SetFrequencySetting (pLine.Replace ("Frequency=", ""));
			}
			else if (pLine.Contains ("LinearWordOrder"))
			{
				settings.SetLinearWordOrder (pLine.Replace ("LinearWordOrder=", ""));
			}
			else if (pLine.Contains ("DisplayLanguage"))
			{
				settings.BetterSetIrishOnly (pLine.Replace ("DisplayLanguage=", ""));
			}
			else if (pLine.Contains ("ModeEndless=True"))
			{
				settings.SetGameMode ("endless");
			}
			else if (pLine.Contains ("ModeTimer=True"))
			{
				settings.SetGameMode ("timer");
			}
			else if (pLine.Contains ("ModeLibraryComplete=True"))
			{
				settings.SetGameMode ("everylastword");
			}
			else if (pLine.Contains ("ModeWordCount=True"))
			{
				settings.SetGameMode ("wordcount");
			}
			else if (pLine.Contains ("TimerDuration"))
			{
				settings.SetTimerLength (pLine.Replace ("TimerDuration=", ""));
			}
			else if (pLine.Contains ("WordCountLength"))
			{
				settings.SetWordCount (pLine.Replace ("WordCountLength=", ""));
			}
			else if (pLine.Contains ("LibraryRepeats"))
			{
				settings.SetLibraryRepeats (pLine.Replace ("LibraryRepeats=", ""));
			}
			else if (pLine.Contains ("CurrentResolution"))
			{
				displaySettings.SetCurrentResolution (pLine.Replace ("CurrentResolution=", ""));
			}
			else if (pLine.Contains ("Fullscreen"))
			{
				displaySettings.SetFullscreen (pLine.Replace ("Fullscreen=", ""));
			}
		

			else if (pLine.Contains ("Audio"))
			{
				engineSettings.SetAudio (pLine.Replace ("Audio=", ""));
			}
		}
	}


	public void
	WriteNewSaveFile()
	{
		List<string> oldSaveArray = ConvertSaveToString ();

		for(int j = 0; j < settings.activeLibraries.Count; j++)
		{
			oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "DefaultLibrary" + (j + 1) + "=" + settings.activeLibraries [j].path);
		}

		for (int j = settings.activeLibraries.Count; j < maxLanes; j++)
		{
			oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "DefaultLibrary" + (j + 1) + "=");
		}


		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "DisregardHyphens=" + settings.disregardHyphens.ToString());
		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "DisregardCapitals=" + settings.disregardCapitals.ToString());
		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "DisregardFullStops=" + settings.disregardFullStops.ToString());
		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "DisregardQuestionMarks=" + settings.disregardQuestionMarks.ToString());
		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "DisregardApostrophes=" + settings.disregardApostrophes.ToString());
		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "Frequency=" + settings.currentFrequency);
		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "LinearWordOrder=" + settings.randomWordOrder.ToString());
		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "DisplayLanguage=" + settings.displayLanguage.ToString ());

		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "ModeEndless=" + settings.endless.ToString());
		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "ModeTimer=" + settings.timedRound.ToString());
		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "TimerDuration=" + settings.timerLength.ToString());
		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "ModeLibraryComplete=" + settings.libraryCompletion.ToString());
		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "LibraryRepeats=" + settings.libraryRepeats.ToString());
		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "ModeWordCount=" + settings.wordCount.ToString());
		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "WordCountLength=" + settings.wordCountLength);
		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "CurrentResolution=" + displaySettings.currentResolution);
		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "Fullscreen=" + displaySettings.fullscreen);

		oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "Audio=" + engineSettings.audioOn.ToString());

		oldSaveArray.RemoveAll (String.IsNullOrEmpty);

		File.WriteAllLines (saveFileName, oldSaveArray.ToArray());
	}

	public List<string>
	OverwriteSaveWithLine(List<string> stringArray, string newLine)
	{
		string lineID = newLine.Split ("=".ToCharArray (), 2) [0];
		bool found = false;

		for(int k = 0; k < stringArray.Count; k++)
		{
			if (stringArray[k].Contains (lineID))
			{
				stringArray[k] = newLine;
				found = true;
			}
		}

		if (found == false)
		{
			stringArray.Add (newLine);
		}

		return stringArray;
	}

	public List<string>
	ConvertSaveToString()
	{
		FileStream saveFile;

		if (File.Exists (saveFileName))
		{
			saveFile = File.OpenRead (saveFileName);
		}
		else
		{
			if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
			{
				Directory.CreateDirectory (saveFilePath);
			}
			saveFile = File.Create (saveFileName);
		}

		saveFile.Close ();

		string rawText = File.ReadAllText (saveFileName);
		List<string> unparsedFile = new List<string>(Regex.Split(rawText, Environment.NewLine));

		return unparsedFile;
	}


//	public void
//	SaveLibraryMetaData(string path)//can probably delete[flag]
//	{
//		if (Application.platform != RuntimePlatform.WebGLPlayer)
//		{
//			List<string> oldSaveArray = ConvertSaveToString ();
//
//			int pathCompletionTimes = 0;
//
//			foreach (string line in oldSaveArray)
//			{
//				if (line.Contains ("Completed<" + path + ">"))
//				{
//					pathCompletionTimes = int.Parse(line.Replace("Completed<" + path + ">=", ""));
//				}
//			}
//
//			pathCompletionTimes++;
//
//			oldSaveArray = OverwriteSaveWithLine (oldSaveArray, "Completed<" + path + ">=" + pathCompletionTimes.ToString ());
//
//			File.WriteAllLines (saveFileName, oldSaveArray.ToArray());
//		}
//	}

	public int
	LoadLibraryMetaData(string path)
	{
		int pathCompletionTimes = 0;

		if (Application.platform != RuntimePlatform.WebGLPlayer)
		{
			List<string> oldSaveArray = ConvertSaveToString ();

			foreach (string line in oldSaveArray)
			{
				if (line.Contains ("Completed<" + path + ">"))
				{
					pathCompletionTimes = int.Parse(line.Replace("Completed<" + path + ">=", ""));
				}
			}
		}

		return pathCompletionTimes;
	}
}