﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CompleteScreen : MonoBehaviour
{
	[SerializeField] LerpScreen completeScreen;
	[SerializeField] ActiveLibraries completeLibraryDisplay;
	[SerializeField] Text wordsCompleteText;
	[SerializeField] Text wordsMissedText;
	[SerializeField] Text timeTakenText;

	[SerializeField] Sprite cuplaFocalSprite;
	[SerializeField] Sprite fainneAirgidSprite;
	[SerializeField] Sprite fainneOirSprite;
	[SerializeField] Sprite seanFhainneSprite;

	[SerializeField] Image cuplaFocal;
	[SerializeField] Image fainneAirgid;
	[SerializeField] Image fainneOir;
	[SerializeField] Image seanFhainne;
}