﻿using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;

public enum GameMode{Endless, Timer, EveryLastWord, Wordcount}
public enum DisplayLanguage{Target, Prompt, Both}


public class GameplaySettings : MonoBehaviour
{
	[Header("References")]
	[SerializeField] GameObject[] settings;
	[SerializeField] GameObject startGameButton;
	[SerializeField] GameObject setDefaultButton;
	[SerializeField] Text freezeText;
	[SerializeField] Text translateText;
	[SerializeField] Text irishOnlyText;
	[SerializeField] Text hyphensText;
	[SerializeField] Text capitalsText;
	[SerializeField] Text fullStopsText;
	[SerializeField] Text questionMarksText;
	[SerializeField] Text apostrophesText;
	[SerializeField] Text frequencyText;
	[SerializeField] Text linearWordOrderText;
	[SerializeField] Text wordCountText;
	[SerializeField] Text timerLengthText;
	[SerializeField] Text libraryRepeatsText;

	[Header("Hard Settings")]
	[SerializeField] public int maxLibraries;
	[SerializeField] public int maxWords = 6;//onscreen?

	[Header("Soft Settings")]
	[SerializeField] Setting freeze;
	[SerializeField] Setting translate;
	[SerializeField] public bool disregardHyphens;
	[SerializeField] public bool disregardCapitals;
	[SerializeField] public bool disregardFullStops;
	[SerializeField] public bool disregardQuestionMarks;
	[SerializeField] public bool disregardApostrophes;
	[SerializeField] public int currentFrequency = 250;
	[SerializeField] public int repeats;
	[SerializeField] public float scrollSpeed = 1;
	[SerializeField] public float currentScrollSpeed = 1;
	[FormerlySerializedAs("linearWordOrder")][SerializeField] public bool randomWordOrder = false;


	[SerializeField] public bool libraryCompletion;
	[SerializeField] public int libraryRepeats;

	[SerializeField] public bool timedRound;
	[SerializeField] public float timerLength = 0f;

	[SerializeField] public bool wordCount;
	[SerializeField] public int wordCountLength;

	[SerializeField] public bool endless;

	[SerializeField] public List<Color> laneColours = new List<Color>();
	[SerializeField] public List<Library> activeLibraries = new List<Library>();


	public void
	SetLinearWordOrder(string newStatus)
	{
		if (newStatus == "True" || newStatus == "False")
			randomWordOrder = bool.Parse (newStatus);
		else
			randomWordOrder = !randomWordOrder;

		RenderSettingsValues();
	}


	[SerializeField] public DisplayLanguage displayLanguage;

	public void
	BetterSetIrishOnly(string newStatus)
	{
		if (newStatus != "")
		{
			displayLanguage = (DisplayLanguage)System.Enum.Parse (typeof(DisplayLanguage), newStatus);
		}
		else if ((int)displayLanguage++ == 2)
		{
			displayLanguage = 0;
		}

		RenderSettingsValues();
	}


	public void
	SetHyphens(string newStatus)
	{
		if (newStatus == "True" || newStatus == "False")
			disregardHyphens = bool.Parse (newStatus);
		else
			disregardHyphens = !disregardHyphens;
		
		RenderSettingsValues();
	}

	public void
	SetCapitals(string newStatus)
	{
		if (newStatus == "True" || newStatus == "False")
			disregardCapitals = bool.Parse (newStatus);
		else
			disregardCapitals = !disregardCapitals;
		
		RenderSettingsValues();
	}


	public void
	SetFullStops(string newStatus)
	{
		if (newStatus == "True" || newStatus == "False")
			disregardFullStops = bool.Parse (newStatus);
		else
			disregardFullStops = !disregardFullStops;
		
		RenderSettingsValues();
	}


	public void
	SetQuestionMarks(string newStatus)
	{
		if (newStatus == "True" || newStatus == "False")
			disregardQuestionMarks = bool.Parse (newStatus);
		else
			disregardQuestionMarks = !disregardQuestionMarks;
		
		RenderSettingsValues();
	}
		

	public void
	SetApostrophes(string newStatus)
	{
		if (newStatus == "True" || newStatus == "False")
			disregardApostrophes = bool.Parse (newStatus);
		else
			disregardApostrophes = !disregardApostrophes;
		
		RenderSettingsValues();
	}

	public void
	SetFreezePowerupSetting(string newStatus)
	{
		if (newStatus == Setting.disabled.ToString ())
			freeze = Setting.disabled;
		else if (newStatus == Setting.enabled.ToString ())
			freeze = Setting.enabled;
		else if (newStatus == Setting.infinite.ToString ())
			freeze = Setting.infinite;
		
		else if (freeze == Setting.disabled)
		{
			freeze = Setting.enabled;
		}
		else if (freeze == Setting.enabled)
		{
			freeze = Setting.infinite;
		}
		else if (freeze == Setting.infinite)
		{
			freeze = Setting.disabled;
		}

		RenderSettingsValues();
	}

	public void
	SetFrequencySetting(string newStatus)
	{
		if (newStatus != "")
			currentFrequency = int.Parse (newStatus);
		else
			currentFrequency += 50;

		if (currentFrequency > 500)
		{
			currentFrequency = 100;
		}

		RenderSettingsValues ();
	}

	public void
	SetTranslatePowerupSetting(string newStatus)
	{
		if (newStatus == Setting.disabled.ToString ())
			translate = Setting.disabled;
		else if (newStatus == Setting.enabled.ToString ())
			translate = Setting.enabled;
		else if (newStatus == Setting.infinite.ToString ())
			translate = Setting.infinite;
		
		else if (translate == Setting.disabled)
		{
			translate = Setting.enabled;
		}
		else if (translate == Setting.enabled)
		{
			translate = Setting.infinite;
		}
		else if (translate == Setting.infinite)
		{
			translate = Setting.disabled;
		}

		RenderSettingsValues();
	}

	public void
	SetLibraryRepeats(string newStatus)
	{
		if (newStatus != "")
			libraryRepeats = int.Parse (newStatus);
		else
			libraryRepeats += 1;

		if (libraryRepeats > 3)
		{
			libraryRepeats = 1;
		}

		RenderSettingsValues();
	}


	public void
	SetTimerLength(string newStatus)
	{
		if (newStatus != "")
			timerLength = int.Parse (newStatus);
		else
			timerLength += 60;

		if (timerLength > 600)
		{
			timerLength = 180;
		}

		RenderSettingsValues();
	}

	public void
	SetWordCount(string newStatus)
	{
		if (newStatus != "")
			wordCountLength = int.Parse (newStatus);
		else
			wordCountLength += 25;

		if (wordCountLength > 300)
		{
			wordCountLength = 50;
		}

		RenderSettingsValues();
	}



	void
	RenderSettingsValues()
	{
		hyphensText.transform.GetChild (0).gameObject.SetActive (disregardHyphens);
		capitalsText.transform.GetChild (0).gameObject.SetActive (disregardCapitals);
		fullStopsText.transform.GetChild (0).gameObject.SetActive (disregardFullStops);
		questionMarksText.transform.GetChild (0).gameObject.SetActive (disregardQuestionMarks);
		apostrophesText.transform.GetChild (0).gameObject.SetActive (disregardApostrophes);
		freezeText.transform.GetChild (0).GetComponent<Text> ().text = freeze.ToString();
		translateText.transform.GetChild (0).GetComponent<Text> ().text = translate.ToString();
		frequencyText.text = (currentFrequency / 5).ToString() + " Words Per Minute";
		linearWordOrderText.transform.GetChild (0).gameObject.SetActive (randomWordOrder);
		irishOnlyText.text = "Display:" + AddSpacesTo(displayLanguage.ToString ());

		string repeatString = "";
		if (libraryRepeats == 1)
		{
			repeatString = "Once";
		}
		else if (libraryRepeats == 2)
		{
			repeatString = "Twice";
		}
		else if (libraryRepeats == 3)
		{
			repeatString = "Thrice";
		}

		wordCountText.text = "Words: " + wordCountLength.ToString ();
		timerLengthText.text = "Timer Length: " + (timerLength / 60).ToString () + " mins";
		libraryRepeatsText.text = "Repeat Library: " + repeatString;

		wordCountText.gameObject.SetActive (wordCount);
		timerLengthText.gameObject.SetActive (timedRound);
		libraryRepeatsText.gameObject.SetActive (libraryCompletion);

	}

	string
	AddSpacesTo(string input)
	{
		string output;

		output = Regex.Replace (input, "[A-Z]", " $0");

		return output;
	}

	void
	Awake()
	{
		DontDestroyOnLoad (transform.gameObject);

		foreach (GameplaySettings settingsObject in GameObject.FindObjectsOfType<GameplaySettings>())
		{
			if (settingsObject != this)
			{
				Destroy (settingsObject.gameObject);
			}
		}

		SetGameMode ("everylastword");

		Input.imeCompositionMode = IMECompositionMode.On;
	}

	public void
	AddLibrary (ref Library newLibrary)
	{

		if (newLibrary.vocabulary.Count > 0)
		{
			bool duplicate = false;
			int removeIndex = 0;

			foreach (Library library in activeLibraries)
			{
				if (library.path == newLibrary.path)
				{
					duplicate = true;
					removeIndex = activeLibraries.IndexOf (library);
				}
			}

			if (duplicate == false)
			{
				if(activeLibraries.Count < maxLibraries)
				activeLibraries.Add (newLibrary);
			}
			else
			{
				activeLibraries.Remove(activeLibraries[removeIndex]);
			}


			if (activeLibraries.Count > 0)
			{
				startGameButton.SetActive (true);
			}
			else
			{
				startGameButton.SetActive (false);
			}
		}

		StartCoroutine(PublishLibraryUpdated ());
	}
		
	public delegate void LibraryUpdated();
	public event LibraryUpdated OnLibraryUpdated;

	IEnumerator
	PublishLibraryUpdated()
	{
		if (OnLibraryUpdated != null)
		{
			OnLibraryUpdated ();
		}
		yield return null;
	}

	public void
	StartGame()
	{
		if (activeLibraries.Count > 0)
		{
			SceneManager.LoadScene ("Main Scene");
		}
	}


	Laneways laneways;


	void
	OnLevelWasLoaded(int level)
	{
		if (level == 1)
		{
			laneways = GameObject.FindObjectOfType<Laneways> ();
			laneways.Initialise(this);
		}
	}

	public void
	SetGameMode(string mode)
	{
		foreach (GameObject setting in settings)
		{
			setting.SetActive (false);
		}

		switch (mode.ToLower())
		{
		case "endless":
			settings [0].SetActive (true);
			DeselectOtherGameModes ();
			endless = true;
			break;

		case "timer":
			settings [1].SetActive (true);
			DeselectOtherGameModes ();
			timedRound = true;
			break;

		case "everylastword":
			settings [2].SetActive (true);
			DeselectOtherGameModes ();
			libraryCompletion = true;
			break;

		case "wordcount":
			settings [3].SetActive (true);
			DeselectOtherGameModes ();
			wordCount = true;
			break;
		}

		RenderSettingsValues ();
	}

	void
	DeselectOtherGameModes()
	{
		endless = false;
		timedRound = false;
		wordCount = false;
		libraryCompletion = false;
	}
}