﻿using UnityEngine;
using System.Collections;

public class EngineSettings : MonoBehaviour
{
	public bool audioOn = true;

	public void
	SetAudio(string newStatus)
	{
		if (newStatus == "True" || newStatus == "False")
			audioOn = bool.Parse (newStatus);
		else
			audioOn = !audioOn;

		if (audioOn)
			AudioListener.volume = 1f;
		else
			AudioListener.volume = 0f;

		RenderSettingsOnScreen ();
	}

	[SerializeField] GameObject audioTick;

	void
	RenderSettingsOnScreen()
	{
		audioTick.SetActive (audioOn);
	}
}