﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Text.RegularExpressions;

public enum PhraseType{English, Singular, Plural, Genitive, GenitivePlural, Dative, Vocative}

public class LibraryDirectoryThumbnail : MonoBehaviour
{
	[Header("References")]
	[SerializeField] GameObject cardFace;
	[SerializeField] Text title;
	[SerializeField] Text author;
	[SerializeField] Image ratingGraphic;
	[SerializeField] GameObject selectedIcon;
	[Header("References: Prompt Settings (Nouns)")]
	[SerializeField] GameObject librarySettings;

//	[SerializeField] bool singularEnabled;
	[SerializeField] Text singularText;
	[SerializeField] Text singularPromptText;
	[SerializeField] GameObject singularPromptTick;

//	[SerializeField] bool pluralEnabled;
	[SerializeField] Text pluralText;
	[SerializeField] Text pluralPromptText;
	[SerializeField] GameObject pluralPromptTick;

//	[SerializeField] bool genitiveEnabled;
	[SerializeField] Text genitiveText;
	[SerializeField] Text genitivePromptText;
	[SerializeField] GameObject genitivePromptTick;

//	[SerializeField] bool genitivePluralEnabled;
	[SerializeField] Text genitivePluralText;
	[SerializeField] Text genitivePluralPromptText;
	[SerializeField] GameObject genitivePluralPromptTick;

//	[SerializeField] bool dativeEnabled;
	[SerializeField] Text dativeText;
	[SerializeField] Text dativePromptText;
	[SerializeField] GameObject dativePromptTick;

//	[SerializeField] bool vocativeEnabled;
	[SerializeField] Text vocativeText;
	[SerializeField] Text vocativePromptText;
	[SerializeField] GameObject vocativePromptTick;


//	[SerializeField] PhraseType singularPrompt;
//	[SerializeField] PhraseType pluralPrompt;
//	[SerializeField] PhraseType genitivePrompt;
//	[SerializeField] PhraseType genitivePluralPrompt;
//	[SerializeField] PhraseType dativePrompt;
//	[SerializeField] PhraseType vocativePrompt;

	[SerializeField] GameObject grammaticalCasesIcon;



	GameplaySettings settings { get { return GameObject.FindObjectOfType<GameplaySettings> (); } }
	VocabPreview vocabPreview { get { return transform.parent.transform.parent.GetComponentInChildren<VocabPreview> (); } }

	public void
	SetSingularPrompt(string newValue)
	{
		SetPrompt (ref library.singularPrompt, newValue);
	}

	public void
	SetPluralPrompt(string newValue)
	{
		SetPrompt (ref library.pluralPrompt, newValue);
	}

	public void
	SetGenitivePrompt(string newValue)
	{
		SetPrompt (ref library.genitivePrompt, newValue);
	}

	public void
	SetGenitivePluralPrompt(string newValue)
	{
		SetPrompt (ref library.genitivePluralPrompt, newValue);
	}

	public void
	SetDativePrompt(string newValue)
	{
		SetPrompt (ref library.dativePrompt, newValue);
	}

	public void
	SetVocativePrompt(string newValue)
	{
		SetPrompt (ref library.vocativePrompt, newValue);
	}

	public void
	SetPrompt (ref PhraseType variable, string newValue)
	{
		if (newValue != "")
		{
			variable = (PhraseType)System.Enum.Parse ((typeof(PhraseType)), newValue);
		}
		else
		{
			if ((int)variable++ == 6) variable = 0;
		}

		library.ResolveMissingCaseErrors (ref variable);

		UpdatePromptValuesUI ();
	}

	public void
	SetSingularEnabled(string newValue)
	{
		SetPromptEnabled (ref library.singularEnabled, newValue);
	}
	public void
	SetPluralEnabled(string newValue)
	{
		SetPromptEnabled (ref library.pluralEnabled, newValue);
	}
	public void
	SetGenitiveEnabled(string newValue)
	{
		SetPromptEnabled (ref library.genitiveEnabled, newValue);
	}
	public void
	SetGenitivePluralEnabled(string newValue)
	{
		SetPromptEnabled (ref library.genitivePluralEnabled, newValue);
	}

	public void
	SetDativeEnabled(string newValue)
	{
		SetPromptEnabled (ref library.dativeEnabled, newValue);
	}

	public void
	SetVocativeEnabled(string newValue)
	{
		SetPromptEnabled (ref library.vocativeEnabled, newValue);
	}

	public void
	SetPromptEnabled(ref bool variable, string newValue)
	{
		if (newValue != "")
		{
			variable = bool.Parse (newValue);
		}
		else
		{
			variable = !variable;
		}

		if (library.ActiveCasesCount () < 1)
		{
			variable = true;
		}

		UpdatePromptValuesUI ();
	}

	void
	Start ()
	{
		ResolveMissingCases ();
	}

	void
	ResolveMissingCases ()
	{
		if (library.ActiveCasesCount () < 1 && library.CheckForAllCases().target != null)
		{
			library.singularEnabled = true;
		}
		if (library.ActiveCasesCount () < 1 && library.CheckForAllCases().targetPlural != null)
		{
			library.pluralEnabled = true;
		}
		if (library.ActiveCasesCount () < 1 && library.CheckForAllCases().targetGenitive != null)
		{
			library.genitiveEnabled = true;
		}
		if (library.ActiveCasesCount () < 1 && library.CheckForAllCases().targetGenitivePlural != null)
		{
			library.genitivePluralEnabled = true;
		}
		if (library.ActiveCasesCount () < 1 && library.CheckForAllCases().targetDative != null)
		{
			library.dativeEnabled = true;
		}
		if (library.ActiveCasesCount () < 1 && library.CheckForAllCases().targetVocative != null)
		{
			library.vocativeEnabled = true;
		}
		UpdatePromptValuesUI ();
	}

		
	void
	UpdatePromptValuesUI()
	{
		CheckWords ();


		if (singularPhrasesPresent)
			genitivePluralText.color = presentColour;
		else
		{
			singularText.color = nonPresentColour;
			library.singularEnabled = false;
		}
		singularPromptTick.SetActive (library.singularEnabled);
		singularPromptText.gameObject.SetActive (library.singularEnabled);
		singularPromptText.text = Regex.Replace (library.singularPrompt.ToString (), "([a-z])([A-Z])", "$1 $2");
		singularPromptText.text = singularPromptText.text.Replace ("Singular", "Self");

		if (pluralPhrasesPresent)
			pluralText.color = presentColour;
		else
		{
			pluralText.color = nonPresentColour;
			library.pluralEnabled = false;
		}
		pluralPromptTick.SetActive (library.pluralEnabled);
		pluralPromptText.gameObject.SetActive (library.pluralEnabled);
		pluralPromptText.text = Regex.Replace (library.pluralPrompt.ToString (), "([a-z])([A-Z])", "$1 $2");
		pluralPromptText.text = pluralPromptText.text.Replace ("Plural", "Self");
		pluralPromptText.text = pluralPromptText.text.Replace ("Genitive Self", "Genitive Plural");

		if (genitivePhrasesPresent)
			genitiveText.color = presentColour;
		else
		{
			genitiveText.color = nonPresentColour;
			library.genitiveEnabled = false;
		}
		genitivePromptTick.SetActive (library.genitiveEnabled);
		genitivePromptText.gameObject.SetActive (library.genitiveEnabled);
		genitivePromptText.text = Regex.Replace (library.genitivePrompt.ToString (), "([a-z])([A-Z])", "$1 $2");
		genitivePromptText.text = genitivePromptText.text.Replace ("Genitive", "Self");
		genitivePromptText.text = genitivePromptText.text.Replace ("Self Plural", "Genitive Plural");

		if (genitivePluralPhrasesPresent)
			genitivePluralText.color = presentColour;
		else
		{
			genitivePluralText.color = nonPresentColour;
			library.genitivePluralEnabled = false;
		}
		genitivePluralPromptTick.SetActive (library.genitivePluralEnabled);
		genitivePluralPromptText.gameObject.SetActive (library.genitivePluralEnabled);
		genitivePluralPromptText.text = Regex.Replace (library.genitivePluralPrompt.ToString (), "([a-z])([A-Z])", "$1 $2");
		genitivePluralPromptText.text = genitivePluralPromptText.text.Replace ("Genitive Plural", "Self");

		if (dativePhrasesPresent)
			dativeText.color = presentColour;
		else
		{
			dativeText.color = nonPresentColour;
			library.dativeEnabled = false;
		}
		dativePromptTick.SetActive (library.dativeEnabled);
		dativePromptText.gameObject.SetActive (library.dativeEnabled);
		dativePromptText.text = Regex.Replace (library.dativePrompt.ToString (), "([a-z])([A-Z])", "$1 $2");
		dativePromptText.text = dativePromptText.text.Replace ("Dative", "Self");

		if (vocativePhrasesPresent)
			vocativeText.color = presentColour;
		else
		{
			vocativeText.color = nonPresentColour;
			library.vocativeEnabled = false;
		}
		vocativePromptTick.SetActive (library.vocativeEnabled);
		vocativePromptText.gameObject.SetActive (library.vocativeEnabled);
		vocativePromptText.text = Regex.Replace (library.vocativePrompt.ToString (), "([a-z])([A-Z])", "$1 $2");
		vocativePromptText.text = vocativePromptText.text.Replace ("Vocative", "Self");

		if (Application.platform != RuntimePlatform.WebGLPlayer)
		{
			SetRatingGraphic ();
		}

		if (Application.platform != RuntimePlatform.WebGLPlayer)
		{
			LibrarySerialiser.WriteSerialisedData (library);
		}

		vocabPreview.SetPreviewText (library);
	}

	[SerializeField] Sprite cuplaFocal;
	[SerializeField] Sprite fainneAirgid;
	[SerializeField] Sprite fainneOir;
	[SerializeField] Sprite seanFhainne;

	void
	SetRatingGraphic()
	{
		if (library.rating == RatingType.None)
			ratingGraphic.color = new Color (0, 0, 0, 0);
		else if (library.rating == RatingType.CuplaFocal)
			ratingGraphic.sprite = cuplaFocal;
		else if (library.rating == RatingType.FainneAirgid)
			ratingGraphic.sprite = fainneAirgid;
		else if (library.rating == RatingType.FainneOir)
			ratingGraphic.sprite = fainneOir;
		else if (library.rating == RatingType.SeanFhainne)
			ratingGraphic.sprite = seanFhainne;
	}

	bool singularPhrasesPresent = false;
	bool pluralPhrasesPresent = false;
	bool genitivePhrasesPresent = false;
	bool genitivePluralPhrasesPresent = false;
	bool dativePhrasesPresent = false;
	bool vocativePhrasesPresent = false;

	[SerializeField] Color presentColour;
	[SerializeField] Color nonPresentColour;

	public void
	CheckWords()
	{
		foreach (Phrase phrase in library.vocabulary)
		{
			if (phrase.target != null)
				singularPhrasesPresent = true;
			if (phrase.targetPlural != null)
				pluralPhrasesPresent = true;
			if (phrase.targetGenitive != null)
				genitivePhrasesPresent = true;
			if (phrase.targetGenitivePlural != null)
				genitivePluralPhrasesPresent = true;
			if (phrase.targetDative != null)
				dativePhrasesPresent = true;
			if (phrase.targetVocative != null)
				vocativePhrasesPresent = true;
		}
	}


	public Library library;

	public void
	Initialise(Library newLibrary)
	{
		title.text = newLibrary.title;
		author.text = newLibrary.author;

		//topRating.image = seanwhatever sprite;

		library = newLibrary;

		//set defaults
		library.singularEnabled = true;


		if (Application.platform != RuntimePlatform.WebGLPlayer)
		{
			LibrarySerialiser.LoadSerialisedData (ref library);
		}

		if (library.CasesInstalled () > 1)
			grammaticalCasesIcon.SetActive (true);

		UpdatePromptValuesUI ();

	}

	public void
	MarkAsSelected(bool newStatus)
	{
		selectedIcon.SetActive (newStatus);
	}
		
	public bool pointerIn {get; set;}


	void
	Update()
	{
		if (Input.GetMouseButtonDown (0) && pointerIn && !librarySettings.activeSelf)
		{
			AddLibrary ();
		}
		else if ((Input.GetMouseButtonDown (0) || Input.GetMouseButtonDown(1)) && !pointerIn)
		{
			ShowLibrarySettings (false);
		}
		else if (Input.GetMouseButtonDown (1) && pointerIn)
		{
			ShowLibrarySettings ();
			if (transform.GetChild (0).GetComponentInChildren<Tooltip> () != null)
			{
				transform.GetChild (0).GetComponentInChildren<Tooltip> ().ShowTooltip (false);
			}
		}
	}

	public void
	AddLibrary()
	{
		settings.AddLibrary (ref library);
	}

	void
	ShowLibrarySettings()
	{
		cardFace.SetActive (!cardFace.activeSelf);
		librarySettings.SetActive (!librarySettings.activeSelf);
	}

	void
	ShowLibrarySettings(bool newStatus)
	{
		cardFace.SetActive (!newStatus);
		librarySettings.SetActive (newStatus);
	}

	public void
	MouseHover()
	{
		vocabPreview.SetPreviewText (library);
	}
}