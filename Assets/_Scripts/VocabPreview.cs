﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class VocabPreview : MonoBehaviour
{
	[SerializeField] RectTransform wordPreviewPrefab;
	[SerializeField] Text wordCount;
	public int tilesAcross = 0;
	public int tilesDown = 0;

	void
	InitialiseRectSettings()
	{
		float tilesAcrossF = GetComponent<RectTransform> ().sizeDelta.x / wordPreviewPrefab.sizeDelta.x;
		tilesAcross = (int)tilesAcrossF;
		float tilesDownF = GetComponent<RectTransform> ().sizeDelta.y / wordPreviewPrefab.sizeDelta.y;
		tilesDown = (int)tilesDownF;
	}

	void
	Awake()
	{
		InitialiseRectSettings ();
		PopulateWordPreviews ();
	}

	void
	PopulateWordPreviews()
	{
		for (int i = 0; i < tilesDown * tilesAcross; i++)
		{
			RectTransform preview = Instantiate (wordPreviewPrefab);
			preview.SetParent (transform, false);

			int spacesAcross = 0;
			int spacesDown = i;

			while (spacesDown > tilesDown - 1)
			{
				spacesDown -= tilesDown;
				spacesAcross++;
			}

			float x = preview.sizeDelta.x * spacesAcross;
			float y = preview.sizeDelta.y * -spacesDown;
			preview.anchoredPosition = new Vector2 (x, y);
		}
	}

	public void
	SetPreviewText(Library library)
	{
		List<Preview> previews = library.GetActivePhrases();
		for (int i = 0; i < transform.childCount; i++)
		{
			if (i < previews.Count)
			{
				if (previews [i].topText.Length > 10)
					transform.GetChild (i).GetChild (0).GetComponent<Text> ().text = previews [i].topText.Remove (8) + "...";
				else
					transform.GetChild (i).GetChild (0).GetComponent<Text> ().text = previews [i].topText;

				if (previews [i].bottomText.Length > 10)
					transform.GetChild (i).GetChild (1).GetComponent<Text> ().text = previews [i].bottomText.Remove (8) + "...";
				else
					transform.GetChild (i).GetChild (1).GetComponent<Text> ().text = previews [i].bottomText;
			}
			else
			{
				transform.GetChild (i).GetChild (0).GetComponent<Text> ().text = "";
				transform.GetChild (i).GetChild (1).GetComponent<Text> ().text = "";
			}
		}
		wordCount.text = library.vocabulary.Count.ToString() + " phrases in this library";
	}
}