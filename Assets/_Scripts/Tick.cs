﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum Setting{disabled, enabled, infinite}

public class Tick : MonoBehaviour
{
	[SerializeField] Text textC;
	[SerializeField] string[] settingOptions;
	int currentSetting = 0;

	public void
	SetTick()
	{
		gameObject.SetActive (!gameObject.activeSelf);
	}


	public void
	SetSetting()
	{
		if (currentSetting == settingOptions.Length - 1)
		{
			currentSetting = 0;
			textC.text = settingOptions[currentSetting];
		}
		else
		{
			currentSetting++;
			textC.text = settingOptions[currentSetting];
		}
	}
}