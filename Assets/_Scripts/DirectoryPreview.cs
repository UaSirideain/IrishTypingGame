﻿using UnityEngine;
using System.Collections;

public class DirectoryPreview : MonoBehaviour
{
	[SerializeField] public Library myLibrary;

	public void
	MouseClick()
	{
		GameObject.FindObjectOfType<GameplaySettings> ().AddLibrary (ref myLibrary);
	}
}