﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class IncorrectLetter : MonoBehaviour
{
	Text text { get { return GetComponent<Text> (); } }

	public void
	RenderWrongLetter(string letter)
	{
		text.text = letter;
		text.color = new Color (.85f, .12f, .12f, 1f);
		StartCoroutine (LerpText ());
	}

	[SerializeField] float seconds;

	int currentTicket = 0;

	IEnumerator
	LerpText()
	{
		currentTicket++;
		int myTicket = currentTicket;

		float lerpTime = 0f;
		while (lerpTime < 1f && myTicket == currentTicket)
		{
			text.color = Color.Lerp (text.color, new Color (.85f, .12f, .12f, 0f), lerpTime);
			lerpTime += Time.deltaTime / seconds;

			yield return null;
		}
	}
}