﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LibraryDirectoryPage : MonoBehaviour
{
	//[Header("References")]
	[SerializeField] GameplaySettings settings;

	[Header("Prefabs")]
	[SerializeField] LibraryDirectoryThumbnail libraryThumbSmallPrefab;
	[SerializeField] LibraryDirectoryFolderThumbnail libraryThumbFolderPrefab;

	[SerializeField] int thumbnailsAcross;


	void
	Awake()
	{
		settings = GameObject.FindObjectOfType<GameplaySettings>();
		settings.OnLibraryUpdated += ShowLibrariesAsSelected;
		float tempThumbnailsAcross = GetComponent<RectTransform> ().sizeDelta.x / libraryThumbSmallPrefab.GetComponent<RectTransform> ().sizeDelta.x;
		thumbnailsAcross = (int)tempThumbnailsAcross;
	}


//	public void
//	DisplayLibraries(List<Library> libraries)
//	{
//		for(int i = 0; i < libraries.Count; i++)	
//		{
//			LibraryDirectoryThumbnail newThumbnail = Instantiate (libraryThumbSmallPrefab);
//			newThumbnail.transform.SetParent (transform, false);
//			newThumbnail.Initialise (libraries[i]);
//
//			ResizeAndColourise (i, newThumbnail.GetComponent<RectTransform> ());
//		}
//	}

	public void
	DisplayLibraries(ArrayList libraries)
	{
		for(int i = 0; i < libraries.Count; i++)	
		{
			if (libraries [i].GetType () == typeof(LibraryFolder))
			{
				LibraryDirectoryFolderThumbnail newThumbnail = Instantiate (libraryThumbFolderPrefab);
				newThumbnail.transform.SetParent (transform, false);
				newThumbnail.Initialise (libraries [i] as LibraryFolder);
				ResizeAndColourise (i, newThumbnail.GetComponent<RectTransform> ());
			}
			else
			{
				LibraryDirectoryThumbnail newThumbnail = Instantiate (libraryThumbSmallPrefab);
				newThumbnail.transform.SetParent (transform, false);
				newThumbnail.Initialise (libraries[i] as Library);

				ResizeAndColourise (i, newThumbnail.GetComponent<RectTransform> ());
			}
		}
	}


	void
	ShowLibrariesAsSelected()
	{
		foreach (LibraryDirectoryThumbnail thumbnail in GetComponentsInChildren<LibraryDirectoryThumbnail>())
		{
			bool wasFound = false;

			foreach (Library activeLibrary in settings.activeLibraries)
			{
				if (thumbnail.library.path == activeLibrary.path)
				{
					wasFound = true;
				}
			}

			thumbnail.MarkAsSelected(wasFound);
		}

	}


	public void
	ResizeAndColourise(int i, RectTransform trans)
	{
		//SIZE AND POSITION
		while (i >= 12)		i -= 12;

		int spacesAcross = i;
		int spacesDown = 0;

		while (spacesAcross > thumbnailsAcross - 1)
		{
			spacesAcross -= thumbnailsAcross;
			spacesDown--;
		}

		float x = trans.sizeDelta.x * spacesAcross;
		float y = trans.sizeDelta.y * spacesDown;
		trans.anchoredPosition = new Vector2 (x, y);


		//COLOUR
		float h = 0.00f;
		float s = 0.60f;
		float v = 0.87f;

		h -= (0.03f * spacesDown);
		h -= (0.01f * spacesAcross);

		if (h < 0) 			h += 1;
		else if (h > 1) 	h -= 1;

		s -= (0.01f * spacesAcross);
		v -= (0.012f * spacesAcross);

		trans.GetComponent<Image> ().color = Color.HSVToRGB (h, s, v);
	}
}