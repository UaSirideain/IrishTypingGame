﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LibraryDirectory : MonoBehaviour
{
	[Header("Settings")]
	[SerializeField] bool folderPage;

	[Header("References")]
	[SerializeField] GameObject previousPageButton;
	[SerializeField] GameObject nextPageButton;

	[Header("Prefabs")]
	[SerializeField] LibraryDirectoryPage pagePrefab;

	[SerializeField]List<LibraryDirectoryPage> currentPageList = new List<LibraryDirectoryPage> ();
	int currentPage = 0;

	void Start()
	{
		if (gameObject.name == "Library Directory")
		{
			List<LibraryFolder> libFolders = LibraryParser.GetInstalledLibraryFolders ();
			List<Library> libs = LibraryParser.GetInstalledLibraries ();


			DisplayLibraries (libFolders, libs);
		}
	}

	public void
	DisplayLibraries(List<LibraryFolder> libFolders, List<Library> libs)
	{
		for (int pageIndex = 0; pageIndex < libFolders.Count + libs.Count; pageIndex += 12)
		{
			LibraryDirectoryPage newPage = Instantiate (pagePrefab);
			newPage.transform.SetParent (transform, false);
			currentPageList.Add(newPage);
			List<Library> newLibraries = new List<Library> ();
			ArrayList newLibs = new ArrayList ();

			for (int i = pageIndex; i < (pageIndex + 12) && i < libFolders.Count + libs.Count; i++)
			{
				if (i < libFolders.Count)
				{
					newLibs.Add (libFolders [i]);
				}
				else
				{
					newLibs.Add (libs [i - libFolders.Count]);
				}
			}

			newPage.DisplayLibraries (newLibs);
		}

		OpenPage (0);
	}

	void
	OpenPage(int i)
	{
		Vector2 closedLeft = new Vector2 (-1920, 35);
		Vector2 open = new Vector2 (0, 35);
		Vector2 closedRight = new Vector2 (1920, 35);;

		if(i > currentPage)
		{
			StartCoroutine(currentPageList[currentPage].GetComponent<LerpScreen>().LerpAway(open, closedLeft));
			StartCoroutine(currentPageList [i].GetComponent<LerpScreen> ().LerpAway(closedRight, open));
		}
		if(i < currentPage)
		{
			StartCoroutine(currentPageList[currentPage].GetComponent<LerpScreen>().LerpAway(open, closedRight));
			StartCoroutine(currentPageList [i].GetComponent<LerpScreen> ().LerpAway(closedLeft, open));
		}
		if (i == currentPage)
		{
			StartCoroutine(currentPageList [i].GetComponent<LerpScreen> ().LerpAway(closedLeft, open));
		}

		currentPage = i;

		if (currentPage > 0 && currentPage < currentPageList.Count - 1)
		{
			previousPageButton.SetActive (true);
			nextPageButton.SetActive (true);
		}
		else if (currentPage == 0 && currentPageList.Count != 1)
		{
			previousPageButton.SetActive (false);
			nextPageButton.SetActive (true);
		}
		else if (currentPageList.Count == 1)
		{
			previousPageButton.SetActive (false);
			nextPageButton.SetActive (false);
		}
		else if (currentPage == currentPageList.Count - 1)
		{
			nextPageButton.SetActive (false);
			previousPageButton.SetActive (true);
		}
	}

	public void
	OpenPageSnappy(int i)
	{
		currentPageList [i].GetComponent<LerpScreen>().LerpAwayInstant();
	}

	public void
	OpenPreviousPage()
	{
		OpenPage (currentPage - 1);
	}

	public void
	OpenNextPage()
	{
		OpenPage (currentPage + 1);
	}

	public void
	FindAndAdd(Library library)
	{
		foreach (LibraryDirectoryThumbnail libraryThumb in GameObject.FindObjectsOfType<LibraryDirectoryThumbnail>())
		{
			if (library.path == libraryThumb.library.path)
			{
				libraryThumb.AddLibrary ();
			}
		}
	}
}