﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tooltip : MonoBehaviour
{
	[Header("References")]
	[SerializeField] GameObject tooltip;

	public void
	ShowTooltip(bool show)
	{
		tooltip.SetActive (show);
	}
}