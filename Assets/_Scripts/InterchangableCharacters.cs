﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InterchangableCharacters : MonoBehaviour
{
	[Header("Optional Disregard")]
	[SerializeField] public List<char> apostrophes = new List<char>();
	[SerializeField] public List<char> questionMarks = new List<char>(); //not interchangable
	[SerializeField] public List<char> fullStops = new List<char>();
	[SerializeField] public List<char> hyphens = new List<char>();
	[SerializeField] public List<char> andSymbols = new List<char>();

	[Header("Always Disregard")]
	[SerializeField] public List<char> brackets = new List<char>();
	[SerializeField] public List<char> currency = new List<char>();
	[SerializeField] public List<char> numbers = new List<char>();
	[SerializeField] public List<char> otherCharacters = new List<char>();

	[Header("Interchangable")]
	[SerializeField] public List<char> characterD = new List<char>();
	[SerializeField] public List<char> characterF = new List<char>();
	[SerializeField] public List<char> characterG = new List<char>();
	[SerializeField] public List<char> characterI = new List<char>();
	[SerializeField] public List<char> characterR = new List<char>();
	[SerializeField] public List<char> characterS = new List<char>();
	[SerializeField] public List<char> characterT = new List<char>();

}