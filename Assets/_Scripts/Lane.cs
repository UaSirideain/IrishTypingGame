﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Lane : MonoBehaviour
{
	GameplaySettings settings { get { return GameObject.FindObjectOfType<GameplaySettings> (); } }
	[SerializeField] List<Phrase> localVocabulary;
	[SerializeField] List<TextTarget> currentWords;
	[SerializeField] GameObject wordPrefab;
	[SerializeField] GameObject freezeIcon;
	public bool isCurrentLane = false;
	bool laneActive = true;
	bool isFrozen = false;

	public float wordLifeTime;

	Library library;
	int libraryIndex;

	[SerializeField] Text wordsLeftCounter;
	[SerializeField] Text completeIndicator;

	void
	Awake()
	{
		wordsLeftCounter.gameObject.SetActive (settings.libraryCompletion);
	}

	float
	CalculateWordLifeTime()
	{
		int charCount = 0;
		foreach (Phrase word in localVocabulary)
		{
			if (word.target.Length > charCount)
			{
				charCount = word.target.Length;
			}
		}

		float life = 60f / (settings.currentFrequency / charCount);
		return life * 3f;
	}


	public void
	SetLibrary(Library newLibrary, int newLibraryIndex)
	{
		library = newLibrary;
		libraryIndex = newLibraryIndex;

		for (int i = 0; i < settings.libraryRepeats; i++)
		{
			foreach (Phrase phrase in library.vocabulary)
			{
				if (phrase.targetOnly /* || has prompt as self */)
				{
					localVocabulary.Add (phrase);
				}
				else
				{
					if (library.singularEnabled && phrase.target != null)
					{
						Phrase localPhrase = new Phrase ();
						localPhrase.target = phrase.target;
						localPhrase.prompt = phrase.GetPrompt (library.singularPrompt, PhraseType.Singular);
						if (library.singularPrompt == PhraseType.Singular)
							localPhrase.targetOnly = true;
						localVocabulary.Add (localPhrase);
					}
					if (library.pluralEnabled && phrase.targetPlural != null)
					{
						Phrase localPhrase = new Phrase ();
						localPhrase.target = phrase.targetPlural;
						localPhrase.prompt = phrase.GetPrompt (library.pluralPrompt, PhraseType.Plural);
						if (library.pluralPrompt == PhraseType.Plural)
							localPhrase.targetOnly = true;
						localVocabulary.Add (localPhrase);
					}
					if (library.genitiveEnabled && phrase.targetGenitive != null)
					{
						Phrase localPhrase = new Phrase ();
						localPhrase.target = phrase.targetGenitive;
						localPhrase.prompt = phrase.GetPrompt (library.genitivePrompt, PhraseType.Genitive);
						if (library.genitivePrompt == PhraseType.Genitive)
							localPhrase.targetOnly = true;
						localVocabulary.Add (localPhrase);
					}
					if (library.genitivePluralEnabled && phrase.targetGenitivePlural != null)
					{
						Phrase localPhrase = new Phrase ();
						localPhrase.target = phrase.targetGenitivePlural;
						localPhrase.prompt = phrase.GetPrompt (library.genitivePluralPrompt, PhraseType.GenitivePlural);
						if (library.genitivePluralPrompt == PhraseType.GenitivePlural)
							localPhrase.targetOnly = true;
						localVocabulary.Add (localPhrase);
					}
					if (library.dativeEnabled && phrase.targetDative != null)
					{
						Phrase localPhrase = new Phrase ();
						localPhrase.target = phrase.targetDative;
						localPhrase.prompt = phrase.GetPrompt (library.dativePrompt, PhraseType.Dative);
						if (library.dativePrompt == PhraseType.Dative)
							localPhrase.targetOnly = true;
						localVocabulary.Add (localPhrase);
					}
					if (library.vocativeEnabled && phrase.targetVocative != null)
					{
						Phrase localPhrase = new Phrase ();
						localPhrase.target = phrase.targetVocative;
						localPhrase.prompt = phrase.GetPrompt (library.vocativePrompt, PhraseType.Vocative);
						if (library.vocativePrompt == PhraseType.Vocative)
							localPhrase.targetOnly = true;
						localVocabulary.Add (localPhrase);
					}
				}
			}
				
			if (localVocabulary[0].targetOnly || settings.displayLanguage == DisplayLanguage.Target)
			{
				RemoveDuplicateVocabEntries (localVocabulary);
			}
		}
			
		GameObject.FindObjectOfType<Laneways> ().IncrementTotalPhrases(localVocabulary.Count, libraryIndex);
		wordLifeTime = CalculateWordLifeTime ();
		RenderWordsLeftCounter ();
	}
		

	List<Phrase>
	RemoveDuplicateVocabEntries(List<Phrase> vocabulary)
	{
		List<int> indicesToRemove = new List<int>();
		foreach (Phrase word in localVocabulary)
		{
			for (int k = vocabulary.IndexOf(word) + 1; k < vocabulary.Count; k++)
			{
				if (word.target == vocabulary [k].target)
				{
					indicesToRemove.Add (k);
				}
			}
		}

		indicesToRemove = indicesToRemove.Distinct ().ToList ();

		for (int l = indicesToRemove.Count - 1; l >= 0; l--)
		{
			vocabulary.RemoveAt(indicesToRemove[l]);
		}

		return vocabulary;
	}

	List<Phrase>
	RandomiseVocabEntries(List<Phrase> vocabulary)
	{
		for(int i = 0; i < vocabulary.Count; i++)
		{				
			Phrase temp = vocabulary[i];
			int randomIndex = Random.Range(i, vocabulary.Count);
			vocabulary[i] = vocabulary[randomIndex];
			vocabulary[randomIndex] = temp;
		}

		return vocabulary;
	}

	public void
	RegisterMissedWord(Phrase missedWord)
	{
		localVocabulary.Add (missedWord);
		transform.parent.GetComponent<Laneways> ().IncrementMissedPhrases (libraryIndex);
	}
		
	public bool busy = false;
	float busyTime;

	public int
	CreateNewWord()
	{
		if (!isFrozen &&
			localVocabulary.Count > 0)
		{
			busy = true;


			GameObject newWord = Instantiate (wordPrefab);
			currentWords.Add (newWord.GetComponent<TextTarget> ());
			newWord.transform.SetParent (transform, false);
			newWord.transform.SetAsFirstSibling ();

			int wordIndex;
			if (settings.randomWordOrder)
			{
				wordIndex = Random.Range (0, localVocabulary.Count);
			}
			else
			{
				wordIndex = 0;
			}

			newWord.GetComponent<TextTarget> ().Initialise (localVocabulary [wordIndex], libraryIndex);
			localVocabulary.Remove (localVocabulary [wordIndex]);


			busyTime = Time.time + ((newWord.GetComponent<RectTransform>().sizeDelta.y * 2) / newWord.GetComponent<TextTarget>().GetScrollSpeed());

			return newWord.GetComponent<TextTarget> ().wordToType.Length;
		}

		return 2;
	}
		
	public bool
	CheckReadyToMakeWord()
	{
		if (busy || !laneActive || isFrozen || !CheckIfClear())
		{
			return false;
		}
			
		return true;
	}

	bool
	CheckIfClear()
	{
		bool clear = false;

		if (currentWords.Count == 0)
		{
			clear = true;
		}
		else if (currentWords [currentWords.Count - 1].GetComponent<RectTransform> ().anchoredPosition.y < -181)//-181 should be replaced with a number calculated based on CPS
		{
			clear = true;
		}

		return clear;
	}


	void
	Update()
	{
		//busy = CheckIfBusy ();
		if (Time.time > busyTime)
		{
			busy = false;
		}

		if (currentWords.Count != 0
		    && isCurrentLane)
		{
			CheckInput ();
		}

//		if (currentWords.Count == 0 && isFrozen == true)
//		{
//			freezeIcon.SetActive (false);
//			isFrozen = false;
//		}

		if (currentWords.Count <= 0 && localVocabulary.Count <= 0)
		{
			CloseLane ();
		}
	}

	void
	CheckInput()
	{
		if (Input.anyKeyDown)
		{
			currentWords[0].CheckInput (Input.inputString);
		}

		PowerupInput ();
		LongPressInput ();
		//MacInput ();

	}

	void
	PowerupInput()
	{
		if (Input.GetButtonDown ("Translate"))
		{
			if (currentWords.Count > 0)
			{
				currentWords [0].Translate ();
			}
		}
//		else if (Input.GetButtonDown ("Freeze") && !isFrozen)
//		{
////			foreach (TextTarget word in currentWords)
////			{
////				word.Freeze (true);
////				isFrozen = true;
////				freezeIcon.SetActive (isFrozen);
////			}
//		}
//		else if (Input.GetButtonDown ("Freeze") && isFrozen)
//		{
////			foreach (TextTarget word in currentWords)
////			{
////				word.Freeze (false);
////				isFrozen = false;
////				freezeIcon.SetActive (isFrozen);
////			}
//		}
		else if (Input.GetKeyDown ("backspace"))
		{
			currentWords [0].CompleteWord (false);
		}
	}


	public void
	Freeze(bool newIsFrozen)
	{
		isFrozen = newIsFrozen;

		foreach (TextTarget word in currentWords)
		{
			word.Freeze (newIsFrozen);
		}
	}



	void
	MacInput()
	{
		if (macAltGr)
		{
			if (Input.GetKeyDown ("a"))
			{
				currentWords [0].CheckInput ("á");
				macAltGr = false;
			}
			if (Input.GetKeyDown ("o"))
			{
				currentWords [0].CheckInput ("ó");
				macAltGr = false;
			}
			if (Input.GetKeyDown ("u"))
			{
				currentWords [0].CheckInput ("ú");
				macAltGr = false;
			}
			if (Input.GetKeyDown ("e"))
			{
				currentWords [0].CheckInput ("é");
				macAltGr = false;
			}
			if (Input.GetKeyDown ("i"))
			{
				currentWords [0].CheckInput ("í");
				macAltGr = false;
			}
		}
		if (Input.GetKey ("right alt") && Input.GetKey("e") && Application.platform == RuntimePlatform.OSXPlayer)
		{
			macAltGr = true;
		}
		if (Input.anyKeyDown)
		{
			macAltGr = false;
		}
	}

	void
	LongPressInput()
	{
		if (Input.GetKeyDown ("a"))
		{
			aHeld = true;
			StartCoroutine (HoldKey ("a"));
		}
		if (Input.GetKeyUp ("a"))
		{
			aHeld = false;
		}
		if (Input.GetKeyDown ("o"))
		{
			aHeld = true;
			StartCoroutine (HoldKey ("o"));
		}
		if (Input.GetKeyUp ("o"))
		{
			aHeld = false;
		}
		if (Input.GetKeyDown ("u"))
		{
			aHeld = true;
			StartCoroutine (HoldKey ("u"));
		}
		if (Input.GetKeyUp ("u"))
		{
			aHeld = false;
		}
		if (Input.GetKeyDown ("e"))
		{
			aHeld = true;
			StartCoroutine (HoldKey ("e"));
		}
		if (Input.GetKeyUp ("e"))
		{
			aHeld = false;
		}
		if (Input.GetKeyDown ("i"))
		{
			aHeld = true;
			StartCoroutine (HoldKey ("i"));
		}
		if (Input.GetKeyUp ("i"))
		{
			aHeld = false;
		}
	}

	bool macAltGr = false;

	IEnumerator
	HoldKey(string key)
	{
		yield return new WaitForSeconds (.18f);
		if (currentWords.Count > 0)
		{
			if (key == "a" && aHeld == true)
			{
				currentWords [0].CheckInput ("á");
			}
			if (key == "o" && aHeld == true)
			{
				currentWords [0].CheckInput ("ó");
			}
			if (key == "u" && aHeld == true)
			{
				currentWords [0].CheckInput ("ú");
			}
			if (key == "e" && aHeld == true)
			{
				currentWords [0].CheckInput ("é");
			}
			if (key == "i" && aHeld == true)
			{
				currentWords [0].CheckInput ("í");
			}
		}
	}

	float aHeldTimeDown;
	float aHeldTimeUp;
	bool aHeld = false;

	public void
	Seppuku(GameObject word)
	{
		currentWords.Remove (word.GetComponent<TextTarget>());

		RenderWordsLeftCounter ();

		if (currentWords.Count == 0 && localVocabulary.Count == 0)
		{
			CloseLane ();
		}
	}

	void
	RenderWordsLeftCounter()
	{
		wordsLeftCounter.text = (localVocabulary.Count + currentWords.Count).ToString();

		if (localVocabulary.Count <= 0 && currentWords.Count <= 0 && wordsLeftCounter.IsActive())
		{
			wordsLeftCounter.gameObject.SetActive (false);
			completeIndicator.gameObject.SetActive (true);
		}
	}

	public void
	SeppukuTwo(GameObject word)
	{
		Destroy (word);
	}

	public void
	SetAsCurrentLane(bool newStatus)
	{
		isCurrentLane = newStatus;
	}


	void
	CloseLane()
	{
		if(settings.libraryCompletion)
		{	
			laneActive = false;
			//lane will lerp til it's 0px wide and other lanes will all lerp by that amount divided by how ever many lanes are left
		}
		else
		{
			SetLibrary (library, libraryIndex);
		}
	}


	public bool
	GetActive()
	{
		return laneActive;
	}
}