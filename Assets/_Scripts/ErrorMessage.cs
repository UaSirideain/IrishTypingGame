﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ErrorMessage : MonoBehaviour
{
	[SerializeField] Image background;
	[SerializeField] Text text;
	[SerializeField] Text headingText;

	public void
	ChangeText(string errorID)
	{
		background.enabled = true;
		text.enabled = true;
		headingText.enabled = true;
		text.text = "id = " + errorID;
	}
}