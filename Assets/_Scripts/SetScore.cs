﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SetScore : MonoBehaviour
{
	[SerializeField] Image cuplaFocal;
	[SerializeField] Image fainneAirgid;
	[SerializeField] Image fainneOir;
	[SerializeField] Image seanFhainne;

	[SerializeField] Sprite cuplaFocalGraphic;
	[SerializeField] Sprite fainneAirgidGraphic;
	[SerializeField] Sprite fainneOirGraphic;
	[SerializeField] Sprite seanFhainneGraphic;

	[SerializeField] Text unscored;


	public void
	GoGo(RatingType rating)
	{
		if (rating >= RatingType.CuplaFocal)
		{
			cuplaFocal.sprite = cuplaFocalGraphic;
			cuplaFocal.color = new Color (1, 1, 1, 1);
		}
		if (rating >= RatingType.FainneAirgid)
		{
			fainneAirgid.sprite = fainneAirgidGraphic;
			fainneAirgid.color = new Color (1, 1, 1, 1);
		}
		if (rating >= RatingType.FainneOir)
		{
			fainneOir.sprite = fainneOirGraphic;
			fainneOir.color = new Color (1, 1, 1, 1);
		}
		if (rating == RatingType.SeanFhainne)
		{
			seanFhainne.sprite = seanFhainneGraphic;
			seanFhainne.color = new Color (1, 1, 1, 1);
		}
		if (rating == RatingType.Unscored)
		{
			cuplaFocal.gameObject.SetActive (false);
			fainneAirgid.gameObject.SetActive (false);
			fainneOir.gameObject.SetActive (false);
			seanFhainne.gameObject.SetActive (false);
			unscored.gameObject.SetActive(true);
		}
	}
}