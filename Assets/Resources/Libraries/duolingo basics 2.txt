﻿Parser Version 1
Name: Basics 2
Author: Duolingo


Format: English/Irish
newspaper/nuachtán
I read/léim
he reads/léann sé
girls/cailíní
boys/buachaillí
men/fir
women/mna
we read/léimid
ye/sibh
milk/bainne
they/siad
rice/rís
sandwich/ceapaire
we eat/ithimid
you/tú
book/leabhar
at me/agam
I have/agam
he has/aige
at him/aige
children/páistí
we drink/ólaimid
at her/aici
she has/aici