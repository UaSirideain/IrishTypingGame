﻿Parser Version 1
Name: Lesson 6
Author: Learning Irish

Format: Irish/English

bróig[pl]bróga/shoe[pl]shoes
bus[pl]busanna/bus[pl]buses
cabhantar[pl]cabhantair/counter[pl]counters
carr[pl]carranna/car[pl]cars
'chuille shórt/everything, every sort of
crann[pl]croinnte/tree[pl]trees
culaith[pl]cultacha/suit[pl]suits
deaide/dad, daddy
éadach[pl]éadaí/cloth[pl]clothes
geansaí[pl]geansaíocha/jumper[pl]jumpers
gúna[pl]gúnaí/dress[pl]dresses
léine[pl]léinteacha/shirt[pl]shirts
maime/mam, mammy
máthair/mother
mórán/many, much
neart/many, lots of
sciorta[pl]sciortaí/skirt[pl]skirts
seachtain[pl]seachtainí/week[pl]weeks
seaicéad[pl]seacéid/jacket[pl]jackets
siopa[pl]siopaí/shop[pl]shops
sráid[pl]sráideanna/street[pl]streets
stoca[pl]stocaí/sock[pl]socks
treabhsar[pl]treabhsair/pair of trousers[pl]pairs of trousers
Baile Átha Cliath/Dublin
cosúil le/like a
daor/expensive, dear
saor/cheap
tinn/sick, sore
ag/at
amáireach/tomorrow
anuraidh/last year
ariamh/ever, never
go minic/often
inné/yesterday
mar sin/so, like that
muise/indeed, now
nó/or
bhí/was, had been
beidh/will be, will have been
ní bheidh/will not be
bhíothadh/one was, people were
beifear/one will be, people will be
ní bheifear/one will not be, people will not be