﻿Parser Version 1
Name: Lesson 9
Author: Learning Irish

Format: Irish/English

bricfásta[pl]bricfástaí/breakfast[pl]breakfasts
buicéad[pl]buicéid/bucket[pl]buckets
caraid[pl]cairde/friend[pl]friends
dánlann[pl]dánlanna/art gallery[pl]art galleries
drama[pl]dramaí/drama, play[pl]plays
dream[pl]dreamanna/crowd, group[pl]crowd, groups
earrach/spring
farraige[pl]farraigí/sea[pl]seas
fáth[pl]fáthanna/reason, cause[pl]reasons, causes
fómhar/autumn
gaineamh/sand
leabharlann[pl]leabharlanna/library[pl]libraries
leitir[pl]leitreacha/letter[pl]letters
maidin[pl]maidineacha/morning[pl]mornings
scáthán[pl]scátháin/mirror[pl]mirrors
sliabh[pl]sléibhte/mountain[pl]mountains
sneachta/snow
spáid[pl]spáideanna/spade[pl]spades
spóirt/sport, fun
teach ósta[pl]tithe ósta/public house, inn[pl]public houses, inns
teas/heat
trá[pl]tránna/strand[pl]strands
creidim/I believe
fairsing/plentiful
glan/clean, clear
náisiúnta/national
te/warm, hot
cé/wh
cén/what is
cé na/what is the
seo/this
sin/that
ar maidin/in the morning
beagnach/almost, nearly
fós/yet, still
go moch/early
i mbliana/this year
maidir leis an/as for the
mar/as
thú/you (as object)
thusa/you (emphasised, as object)
é/him
eisean/him (emphasised)
í/her
ise/her (emphasised)
iad/them
iadsan/them (emphasised)