﻿Parser Version 1
Name: Lesson 8
Author: Learning Irish

Format: Irish/English

aois/age, century
coicís/fortnight
cois[pl]cosa/leg, foot[pl]legs, feet
croí[pl]croítí/heart[pl]hearts
deartháir[pl]deartháracha/brother[pl]brothers
driofúr[pl]driofúracha/sister[pl]sisters
eochair[pl]eochracha/key[pl]keys
lá[pl]laethanta/day[pl]days
óige/youth, childhood
posta[pl]postaí/job, post; post office[pl]jobs, post
scéal[pl]scéalta/story, news[pl]stories, news
scoil[pl]scoileanna/school[pl]schools
súil[pl]súile/eye[pl]eyes
tráthnóna[pl]tráthnónaí/evening[pl]evenings
uair[pl]uaireanta/time, hour[pl]sometimes
úlla[pl]úllaí/apple[pl]apples
cloisim/I hear
sílim/I think
tigim/I understand
cantalach/bad-humoured, cranky
ceart/right, correct
dána/bold
fliuch/wet
marbh/dead
sean/old
tuirseach/tired
uilig/entire(ly), all, altogether
arú amáireach/the day after tomorrow
arú anuraidh/the year before last
arú inné/the day before yesterday
go háirithe/especially
go hiondúil/usually
má/if
mara/if not, unless