﻿Parser Version 1
Name: Míonna
Author: Digital Press

Format: English/Irish [declensions]

January / Eanáir [gn]mí Eanáir
February / Feabhra [gn]mí Feabhra
March / Márta [gn]mí an Mhárta
April / Aibreán [gn]mí Aibreáin
May / Bealtaine [gn]mí na Bealtaine
June / Meitheamh [gn]mí an Mheithimh
July / Iúil [gn]mí Iúil
August / Lúnasa [gn]mí Lúnasa
September / Meán Fómhair [gn]mí Mheán Fómhair
October / Deireadh Fómhair [gn]mí Dheireadh Fómhair
November / Samhain [gn]mí na Samhna
December / Nollaig [gn]mí na Nollag


Winter / an geimhreadh
Spring / an t-earrach
Summer / an samhradh
Autumn / an fómhar