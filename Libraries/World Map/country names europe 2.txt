﻿Parser Version 1
Name: European Countries 2
Author: Digital Press

Format: English/Irish

Latvia/An Laitvia
Lithuania/An Liotuáin
Belarus/An Bhealarúis
Ukraine/An Úcráin
Moldova/An Mholdóiv
Romania/An Rómáin
Bulgaria/An Bhulgáir
Kosovo/An Chosaiv
Serbia/An tSeirbia
Croatia/An Chróit
Slovenia/An tSlóvéin
Montenegro/Montaineagró
Belgium/An Bheilg
Luxembourg/Lucsamburg
Norway/An Iorua
Iceland/An Íoslainn
Cyprus/An Chipir
Malta/Málta
Austria/An Ostair
Czech Republic/Poblacht na Seice
Slovakia/An tSlóvaic
Poland/An Pholainn
Switzerland/An Eilvéis
Macedonia/An Mhacadón